﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ExTStudio.Admin
{
    public partial class checkin_2 : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.QueryString["ProbId"] != null)
                        {
                            BindData();
                        }
                        else
                        {
                            Response.Redirect("~/Admin/check-in.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        private void BindData()
        {
            try
            {
                objvalue.Member_Id = Convert.ToInt64(Request.QueryString["ProbId"].ToString());
                ds = objInteraction.Membership_Select_By_Id_For_Trainer(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtSName.Text = ds.Tables[0].Rows[0]["Student_Name"].ToString();
                        if (ds.Tables[0].Rows[0]["BOD"].ToString() != "")
                        {
                            DateTime DBod = new DateTime();
                            String s = ds.Tables[0].Rows[0]["BOD"].ToString();
                            DBod = Convert.ToDateTime(s);
                            txtBod.Text = DBod.ToString("yyyy-MM-dd");

                            txtBelt.Text = ds.Tables[0].Rows[0]["Belt"].ToString();

                            DateTime DNBTDate = new DateTime();
                            String s1 = ds.Tables[0].Rows[0]["NBTDate"].ToString();
                            DNBTDate = Convert.ToDateTime(s1);
                            txtNBTDate.Text = DNBTDate.ToString("yyyy-MM-dd");

                            txtNBTTime.Text = ds.Tables[0].Rows[0]["NBTTime"].ToString();
                        }
                        else
                        {
                            txtBod.Text = txtBelt.Text = txtNBTDate.Text = txtNBTTime.Text = "";
                        }
                    }
                    else
                    {
                        txtSName.Text = txtBod.Text = txtBelt.Text = txtNBTDate.Text = txtNBTTime.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion


        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                objvalue.Member_Id = Convert.ToInt64(Request.QueryString["ProbId"].ToString());
                objvalue.Full_Name = txtSName.Text;
                objvalue.BOD = Convert.ToDateTime(txtBod.Text);
                objvalue.Belt = txtBelt.Text;
                objvalue.NBTDate = Convert.ToDateTime(txtNBTDate.Text);
                objvalue.NBTTime = txtNBTTime.Text;
                objvalue.Flag = "A";
                objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                rowEffectedarr = objInteraction.Check_In_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    // Response.Redirect("~/Admin/membership.aspx");
                    BindData();
                    ClearControl();
                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }


        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtBod.Text = txtBelt.Text = txtNBTDate.Text = txtNBTTime.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}