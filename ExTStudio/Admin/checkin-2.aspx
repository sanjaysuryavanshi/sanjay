﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="checkin-2.aspx.cs" Inherits="ExTStudio.Admin.checkin_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var SName = document.getElementById('<%=txtSName.ClientID%>').value;
            var BOD = document.getElementById('<%=txtBod.ClientID%>').value;
            var Belt = document.getElementById('<%=txtBelt.ClientID%>').value;
            var NBT = document.getElementById('<%=txtNBTDate.ClientID%>').value;
            var NBT = document.getElementById('<%=txtNBTTime.ClientID%>').value;

            if (SName == "" || SName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter student name.';
                document.getElementById('<%=txtSName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (BOD == "" || BOD == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter date of birth.';
                document.getElementById('<%=txtBod.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Belt == "" || Belt == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter belt.';
                document.getElementById('<%=txtBelt.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (NBT == "" || NBT == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter next bell test date.';
                document.getElementById('<%=txtNBTDate.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (NBT == "" || NBT == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter next bell test time.';
                document.getElementById('<%=txtNBTTime.ClientID%>').focus();
                HideLabel();
                return false;
            }


    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper" style="min-height: 232px;">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Check In</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">

            <div class="course-main">
                <div class="row">
                    <div class="col-lg-8 sapana">
                        <div class="card-outline-info">

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtSName" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Date of Birth</label>
                                            <%-- <input type="date" class="form-control" placeholder="dd/mm/yyyy">--%>
                                            <asp:TextBox ID="txtBod" TextMode="Date" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Belt</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtBelt" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Next Bell Test</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtNBTDate" TextMode="Date" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtNBTTime" TextMode="Time" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <%-- <button class="chexk-2 chexk-2-2">Submit</button>--%>
                                        <asp:LinkButton runat="server" ID="lbtnSave" class="chexk-2 chexk-2-2" Text="Submit" OnClick="lbtnSave_Click" OnClientClick="return validateForm()"></asp:LinkButton>
                                    </div>
                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 check-next">
                                        <%--<a href="membership.aspx" class="next">Next</a>--%>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</asp:Content>
