﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="membership.aspx.cs" Inherits="ExTStudio.Admin.membership" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function AllowAlphabet(sender) {
            if (!sender.value.match(/^[a-zA-Z]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                // toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();
            }
        }
        function AllowAlphabetSpace(sender) {
            if (!sender.value.match(/^[a-zA-Z ._-]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();

            }
        }

        function validateMobileNo(form) {
            var tin = form.value;
            if (!isNumeric(form)) {
                return false;
            }
            else if (!(tin.length == 10)) {

                //  $.gritter.add({ title: 'Error Message', text: 'Please enter 10 digit Mobile No.' });
                //alert("Please enter 10 digit Mobile No.");

                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter 10 digit Mobile No.';
                form.value = "";
                form.focus();
                HideLabel();
                return false;
            }
        return true;
    }

    function validatenumerics(key) {
        var keycode = (key.which) ? key.which : key.keyCode;
        if (keycode > 31 && (keycode < 48 || keycode > 57)) {
            return false;
        }
        else return true;
    }

    function checkEmail(sender) {
        var email = sender.value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            sender.value = "";
            sender.focus();
            //alert('Please Provide Proper Email Address');
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
            HideLabel();

            //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol required");
            return false;
        }
        else {
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
            return true();
        }
    }
    </script>

    <script>
        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            <%--var Name = document.getElementById('<%=txtName.ClientID%>').value;--%>
            var SName = document.getElementById('<%=txtStudentName.ClientID%>').value;
            var SAddress = document.getElementById('<%=txtAddress.ClientID%>').value;
            var Zip = document.getElementById('<%=txtZip.ClientID%>').value;
            var Country = document.getElementById('<%=txtCountry.ClientID%>').value;
            var MobileNo = document.getElementById('<%=txtMobileNo.ClientID%>').value;
            var IBANS = document.getElementById('<%=txtIBANS.ClientID%>').value;
            var Email = document.getElementById('<%=txtEmailId.ClientID%>').value;

            if (document.getElementById('<%=ddlTrainingTimes.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select training times.';
                document.getElementById('<%=ddlTrainingTimes.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }
            else if (document.getElementById('<%=ddlTrainer.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select trainer .';
                document.getElementById('<%=ddlTrainer.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }
            <%-- else if (Name == "" || Name == undefined) {
                 document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                 document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter name.';
                 document.getElementById('<%=txtName.ClientID%>').focus();
                 HideLabel();
                 return false;
             }--%>
            else if (SName == "" || SName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter student name.';
                document.getElementById('<%=txtStudentName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (SAddress == "" || SAddress == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter address.';
                document.getElementById('<%=txtAddress.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Zip == "" || Zip == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter zip.';
                document.getElementById('<%=txtZip.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Country == "" || Country == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter country.';
                document.getElementById('<%=txtCountry.ClientID%>').focus();
                HideLabel();
                return false;
            }

            else if (MobileNo == "" || MobileNo == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter mobile no.';
                document.getElementById('<%=txtMobileNo.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (IBANS == "" || IBANS == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter IBANS.';
                document.getElementById('<%=txtIBANS.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Email == "" || Email == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter Email Address.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper" style="min-height: 232px;">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add New Member</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">

            <div class="course-main">
                <div class="row">
                    <div class="col-lg-12 sapana">
                        <div class="card-outline-info">

                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Photo</label>
                                            <asp:FileUpload ID="fuPhoto" class="form-control ImageFile"
                                                runat="server"></asp:FileUpload>
                                        </div>
                                    </div>
                                </div>

                                <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                                            <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                                <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlTrainer" />
                                    </Triggers>
                                    <ContentTemplate>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Trainer Name</label>
                                                    <%--<input type="text" class="form-control">--%>
                                                    <%-- <asp:TextBox ID="txtName" class="form-control" runat="server" MaxLength="100"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlTrainer" class="form-control" runat="server" OnSelectedIndexChanged="ddlTrainer_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <%-- </div>

                                        <div class="row">--%>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Training Times</label>
                                                    <asp:DropDownList ID="ddlTrainingTimes" class="form-control" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name Of Student</label>
                                            <asp:TextBox ID="txtStudentName" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <asp:TextBox ID="txtAddress" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Pincode</label>
                                            <asp:TextBox ID="txtZip" class="form-control" runat="server" onkeypress="return validatenumerics(event)" MaxLength="6" placeholder="Enter Zip"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Country</label>
                                            <asp:TextBox ID="txtCountry" class="form-control" runat="server" placeholder="Enter Country"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Telephone</label>
                                            <asp:TextBox ID="txtMobileNo" class="form-control" runat="server" onkeypress="return validatenumerics(event)" MaxLength="10" placeholder="Enter Tel"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email Id</label>
                                            <asp:TextBox ID="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)" MaxLength="50" placeholder="Enter Email Address"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">IBAN</label>
                                            <asp:TextBox ID="txtIBANS" class="form-control" runat="server" placeholder="Enter IBAN"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 check-next">
                                        <%-- <a href="#" class="next">Next</a>--%>
                                        <asp:LinkButton runat="server" ID="lbtnNext" class="next" Text="Next" OnClientClick="return validateForm()" OnClick="lbtnNext_Click"></asp:LinkButton>
                                        <asp:HiddenField runat="server" ID="hnId" />
                                        <asp:HiddenField runat="server" ID="hnImagePath" />
                                    </div>
                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 sapana">
                                    <div class="card-outline-info">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Trainer Name</th>
                                                                    <th>Training Times</th>
                                                                    <th>Student Name</th>
                                                                    <th>Package Name</th>
                                                                    <th>action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <asp:ListView ID="repMember" runat="server" OnItemCommand="repMember_ItemCommand">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.DataItemIndex + 1 %></td>
                                                                            <td><%# Eval("Trainer_Name")%></td>
                                                                            <td><%# Eval("Locations")%></td>
                                                                            <td><%# Eval("Student_Name")%></td>
                                                                            <td><%# Eval("Package_Name")%></td>
                                                                            <td>
                                                                                <div class="plan-right-btn">
                                                                                    <asp:LinkButton ID="lbtnAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "A" ? true: false %>' OnClientClick="return confirm('Are you sure want to Deactive this Record?')" CommandName="Inactive" CssClass="courcebtn courcebtn-btn" CommandArgument='<%#Eval("Member_Id") %>'>Inactive</asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbtniAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "D" ? true: false %>' CommandName="Active" CssClass="coursebtn2 course-btn3" CommandArgument='<%#Eval("Member_Id") %>'>Active</asp:LinkButton>
                                                                                </div>
                                                                                <div class="plan-left-btn">
                                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Modify" CssClass="coursebtn course-btn2" CommandArgument='<%#Eval("Member_Id") %>'>Edit</asp:LinkButton>
                                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="ModifyP" CssClass="coursebtn course-btn2" CommandArgument='<%#Eval("Member_Id") %>'>Edit Package</asp:LinkButton>
                                                                                </div>

                                                                            </td>
                                                                        </tr>

                                                                    </ItemTemplate>
                                                                </asp:ListView>


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="page_count pagination-top pagination-main">
                                                <nav aria-label="Page navigation">
                                                    <asp:DataPager ID="dpPagingContactTop" runat="server" PagedControlID="repMember" PageSize="5"
                                                        class="pagination people-pagination" OnPreRender="dpPagingContactTop_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="tableActivePage" NextPageText="»" PreviousPageText="«" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </nav>
                                            </div>

                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
