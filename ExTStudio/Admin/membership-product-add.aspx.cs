﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class membership_product_add : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.QueryString["MOptionId"] != null)
                        {
                            BindData();
                        }
                        else
                        {
                            Response.Redirect("~/Admin/membership-options.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }



        #region Bind Data
        public void BindData()
        {
            try
            {
                objvalue.M_Option_Id = Convert.ToInt64(Request.QueryString["MOptionId"].ToString());
                ds = objInteraction.Membership_Product_Select_By_MOptionId(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        repProduct.DataSource = ds.Tables[0];
                        repProduct.DataBind();
                    }
                    else
                    {
                        repProduct.DataSource = null;
                        repProduct.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (lbtnSave.Text == "Save")
                {
                    #region File Upload
                    String UploadFile;
                    if (fuPhoto.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                        string AddressPath = Server.MapPath("../images/MemberProduct");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("../images/MemberProduct/" + ProfileImage1);
                        UploadFile = "../images/MemberProduct/" + ProfileImage1;
                        fuPhoto.SaveAs(Server.MapPath(UploadFile));
                        objvalue.ImagePath = UploadFile;
                    }
                    else
                    {
                        objvalue.ImagePath = "../images/no-image.png";
                    }

                    #endregion

                    objvalue.Product_Name = txtProductName.Text;
                    objvalue.M_Option_Id = Convert.ToInt64(Request.QueryString["MOptionId"].ToString());
                    objvalue.Flag = "A";
                    objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Membership_Product_Insert(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (lbtnSave.Text == "Update")
                {
                    #region File Upload
                    String UploadFile;
                    if (fuPhoto.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                        string AddressPath = Server.MapPath("../images/MemberProduct");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("../images/MemberProduct/" + ProfileImage1);
                        UploadFile = "../images/MemberProduct/" + ProfileImage1;
                        fuPhoto.SaveAs(Server.MapPath(UploadFile));
                        objvalue.ImagePath = UploadFile;
                    }
                    else
                    {
                        objvalue.ImagePath = hdnImahePath.Value;
                    }

                    #endregion

                    objvalue.Product_Name = txtProductName.Text;
                    objvalue.M_Option_Id = Convert.ToInt64(Request.QueryString["MOptionId"].ToString());
                    objvalue.Flag = "A";
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.M_Product_Id = Convert.ToInt64(hdnId.Value);

                    rowEffectedarr = objInteraction.Membership_Product_Update(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtProductName.Text = "";
                lbtnSave.Text = "Save";
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void repProduct_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (e.CommandName == "Inactive")
                {
                    objvalue.M_Product_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "D";

                    rowEffectedarr = objInteraction.Membership_Product_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Inactivated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (e.CommandName == "Active")
                {
                    objvalue.M_Product_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "A";

                    rowEffectedarr = objInteraction.Membership_Product_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Activated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (e.CommandName == "Modify")
                {
                    //Response.Redirect("~/Admin/membership-options-kids.aspx?MOptionId=" + e.CommandArgument);
                    objvalue.M_Product_Id = Convert.ToInt32(e.CommandArgument);
                    ds = objInteraction.Membership_Product_Select_By_Id(objvalue);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            txtProductName.Text = ds.Tables[0].Rows[0]["Product_Name"].ToString();
                            hdnMOptionId.Value = ds.Tables[0].Rows[0]["M_Option_Id"].ToString();
                            hdnId.Value = ds.Tables[0].Rows[0]["M_Product_Id"].ToString();
                            hdnImahePath.Value = ds.Tables[0].Rows[0]["ImagePath"].ToString();
                            lbtnSave.Text = "Update";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
    }
}