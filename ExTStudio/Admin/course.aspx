﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="course.aspx.cs" Inherits="ExTStudio.Admin.check_in2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';


            var Name = document.getElementById('<%=txtCourseName.ClientID%>').value;

            if (Name == "" || Name == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter course name.';
                document.getElementById('<%=txtCourseName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (document.getElementById('<%=ddlLocation.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select location.';
                document.getElementById('<%=ddlLocation.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }

        return true;
    }
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper" style="min-height: 232px;">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Course</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="traning-white-box">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-outline-info">

                            <div class="card-body">
                                <div class="form-body">
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Course Name</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtCourseName" class="form-control" runat="server"
                                                MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Location</label>
                                            <%--<select class="form-control custom-select">
                                                <option>--Select your Country--</option>
                                                <option>India</option>
                                                <option>Sri Lanka</option>
                                                <option>USA</option>
                                            </select>--%>
                                            <asp:DropDownList ID="ddlLocation" class="form-control custom-select" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:LinkButton runat="server" ID="lbtnSave" class="btn btn-success" Text="Save" OnClick="lbtnSave_Click" OnClientClick="return validateForm()"></asp:LinkButton>
                                            <asp:HiddenField runat="server" ID="hnId" />
                                        </div>
                                    </div>


                                    <!--/span-->
                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Course Name</th>
                                                        <th>Location Name</th>
                                                        <th>action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <asp:ListView ID="repCourse" runat="server" OnItemCommand="repCourse_ItemCommand">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.DataItemIndex + 1 %></td>
                                                                <td><%# Eval("Course_Name")%></td>
                                                                <td><%# Eval("Location_Name")%></td>
                                                                <td>
                                                                    <div class="plan-right-btn">
                                                                        <asp:LinkButton ID="lbtnAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "A" ? true: false %>' OnClientClick="return confirm('Are you sure want to Deactive this Record?')" CommandName="Inactive" CssClass="courcebtn courcebtn-btn" CommandArgument='<%#Eval("Course_Id") %>'>Inactive</asp:LinkButton>
                                                                        <asp:LinkButton ID="lbtniAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "D" ? true: false %>' CommandName="Active" CssClass="coursebtn2 course-btn3" CommandArgument='<%#Eval("Course_Id") %>'>Active</asp:LinkButton>
                                                                    </div>
                                                                    <div class="plan-left-btn">
                                                                       
                                                                         <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Modify" CssClass="coursebtn course-btn2" CommandArgument='<%#Eval("Course_Id") %>'>Edit</asp:LinkButton>
                                                                    </div>

                                                                </td>
                                                            </tr>

                                                        </ItemTemplate>
                                                    </asp:ListView>

                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
    </div>

</asp:Content>
