﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="company-detail.aspx.cs" Inherits="ExTStudio.Admin.company_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgLogoImage.ClientID%>').prop('src', e.target.result)
                    .css('visibility', 'visible')
                };
                reader.readAsDataURL(input.files[0]);
                }
            }
    </script>

    <script>
        function validatenumerics(key) {
            var keycode = (key.which) ? key.which : key.keyCode;
            if (keycode > 31 && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else return true;
        }

        function validateMobileNo(form) {
            var tin = form.value;
            if (!isNumeric(form)) {
                return false;
            }
            else if (!(tin.length == 10)) {

                //  $.gritter.add({ title: 'Error Message', text: 'Please enter 10 digit Mobile No.' });
                //alert("Please enter 10 digit Mobile No.");

                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter 10 digit Mobile No.';
                form.value = "";
                form.focus();
                HideLabel();
                return false;
            }
        return true;
    }

    function checkEmail(sender) {
        var email = sender.value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            sender.value = "";
            sender.focus();
            //alert('Please Provide Proper Email Address');
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
            HideLabel();

            //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol required");
            return false;
        }
        else {
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
            return true();
        }
    }
    </script>

    <script>
        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var CompanyName = document.getElementById('<%=txtCompanyName.ClientID%>').value;
            var Website = document.getElementById('<%=txtWebsite.ClientID%>').value;
            var EmailId = document.getElementById('<%=txtEmailId.ClientID%>').value;
            var Mobile = document.getElementById('<%=txtMobile.ClientID%>').value;
            var Add1 = document.getElementById('<%=txtAddressLine1.ClientID%>').value;
            var Add2 = document.getElementById('<%=txtAddressLine2.ClientID%>').value;
            var Add3 = document.getElementById('<%=txtAddressLine3.ClientID%>').value;
            var Add4 = document.getElementById('<%=txtAddressLine4.ClientID%>').value;
            var Add5 = document.getElementById('<%=txtAddressLine5.ClientID%>').value;
            var City = document.getElementById('<%=txtCity.ClientID%>').value;
            var State = document.getElementById('<%=txtState.ClientID%>').value;
            var Country = document.getElementById('<%=txtCountry.ClientID%>').value;
            var Pincode = document.getElementById('<%=txtPincode.ClientID%>').value;

            if (CompanyName == "" || CompanyName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter company name.';
                document.getElementById('<%=txtCompanyName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Website == "" || Website == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter website link.';
                document.getElementById('<%=txtWebsite.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (EmailId == "" || EmailId == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter emailid.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Mobile == "" || Mobile == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter mobile no.';
                document.getElementById('<%=txtMobile.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Add1 == "" || Add1 == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter address line 1.';
                document.getElementById('<%=txtAddressLine1.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Add2 == "" || Add2 == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter address line 2.';
                document.getElementById('<%=txtAddressLine2.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Add3 == "" || Add3 == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter address line 3.';
                document.getElementById('<%=txtAddressLine3.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Add4 == "" || Add4 == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter address line 4.';
                document.getElementById('<%=txtAddressLine4.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Add5 == "" || Add5 == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter address line 5.';
                document.getElementById('<%=txtAddressLine5.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (City == "" || City == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter city.';
                document.getElementById('<%=txtCity.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (State == "" || State == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter state.';
                document.getElementById('<%=txtState.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Country == "" || Country == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter country.';
                document.getElementById('<%=txtCountry.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Pincode == "" || Pincode == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter pincode.';
                document.getElementById('<%=txtPincode.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Company Detail</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">

                            <div class="form-body">



                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <%-- <input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtCompanyName" class="form-control" runat="server" MaxLength="100" placeholder="Enter Company Name"></asp:TextBox>

                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Company Short Name</label>
                                            <asp:TextBox ID="txtCompanyShortName" class="form-control" runat="server" MaxLength="10" placeholder="Enter Company Short Name"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Website</label>
                                            <asp:TextBox ID="txtWebsite" class="form-control" runat="server" MaxLength="50" placeholder="Enter Website"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>EmailId</label>
                                            <asp:TextBox ID="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)" MaxLength="50" placeholder="Enter Email Address"></asp:TextBox>

                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Office</label>
                                            <asp:TextBox ID="txtOffice" class="form-control" runat="server" MaxLength="15" placeholder="Enter Office No."></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <asp:TextBox ID="txtMobile" class="form-control" runat="server" onchange="validateMobileNo(this)" onkeypress="return validatenumerics(event)" MaxLength="10" placeholder="Enter Mobile No."></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address Line 1</label>
                                            <asp:TextBox ID="txtAddressLine1" class="form-control" runat="server" MaxLength="100" placeholder="Enter Address Line 1"></asp:TextBox>

                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address Line 2</label>
                                            <asp:TextBox ID="txtAddressLine2" class="form-control" runat="server" MaxLength="100" placeholder="Enter Address Line 2"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address Line 3</label>
                                            <asp:TextBox ID="txtAddressLine3" class="form-control" runat="server" MaxLength="50" placeholder="Enter Street"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address Line 4</label>
                                            <asp:TextBox ID="txtAddressLine4" class="form-control" runat="server" MaxLength="50" placeholder="Enter Nearby"></asp:TextBox>

                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address Line 5</label>
                                            <asp:TextBox ID="txtAddressLine5" class="form-control" runat="server" MaxLength="50" placeholder="Enter Area"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>City</label>
                                            <asp:TextBox ID="txtCity" class="form-control" runat="server" MaxLength="50" placeholder="Enter City"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>State</label>
                                        <asp:TextBox ID="txtState" class="form-control" runat="server" MaxLength="50" placeholder="Enter State"></asp:TextBox>

                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <asp:TextBox ID="txtCountry" class="form-control" runat="server" MaxLength="50" placeholder="Enter Country"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Pincode</label>
                                        <asp:TextBox ID="txtPincode" class="form-control" runat="server" onkeypress="return validatenumerics(event)" MaxLength="6" placeholder="Enter Pincode"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Logo</label>
                                                <asp:FileUpload ID="fuLogo" class="form-control ImageFile" onchange="ShowImagePreview(this)" runat="server"></asp:FileUpload>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <img src="../images/no-image.png" id="imgLogoImage" class="ImagePrivew form-control" alt="" style="width: 150px; height: 150px" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                            </div>

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                            <asp:LinkButton runat="server" ID="lbtnSave" class="next" Text="Save" OnClientClick="return validateForm()" OnClick="lbtnSave_Click"></asp:LinkButton>
                                            <asp:HiddenField ID="hnImagePath" runat="server" />
                                            <asp:HiddenField ID="hnId" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
