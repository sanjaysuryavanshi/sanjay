﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="membership-options-kids-list.aspx.cs" Inherits="ExTStudio.Admin.membership_options_kids_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper" style="min-height: 232px;">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Membership options/kids</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="traning-white-box">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-outline-info">

                            <div class="card-body">
                                <div class="form-body">
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="membership-options-kids.aspx" class="next">Add New</a>
                                    </div>
                                    <!--/span-->
                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Starter Price</th>
                                                        <th>Package Name</th>
                                                        <th>Package Time</th>
                                                        <th>Times Per Week</th>
                                                        <th>Monthly Investment</th>
                                                        <th>View Product</th>
                                                        <th>action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <asp:ListView ID="repOption" runat="server" OnItemCommand="repOption_ItemCommand">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.DataItemIndex + 1 %></td>
                                                                <td><%# Eval("Starter_Price")%></td>
                                                                <td><%# Eval("Package_Name")%></td>
                                                                <td><%# Eval("Package_Time")%></td>
                                                                <td><%# Eval("Times_Per_Week")%></td>
                                                                <td><%# Eval("Monthly_Investment")%></td>
                                                                <td>
                                                                    <a href="membership-product-add.aspx?MOptionId=<%# Eval("M_Option_Id") %>" class="courcebtn courcebtn-btn">Edit Product</a>
                                                                </td>
                                                                <td>
                                                                    <div class="plan-right-btn">
                                                                        <asp:LinkButton ID="lbtnAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "A" ? true: false %>' OnClientClick="return confirm('Are you sure want to Deactive this Record?')" CommandName="Inactive" CssClass="courcebtn courcebtn-btn" CommandArgument='<%#Eval("M_Option_Id") %>'>Inactive</asp:LinkButton>
                                                                        <asp:LinkButton ID="lbtniAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "D" ? true: false %>' CommandName="Active" CssClass="coursebtn2 course-btn3" CommandArgument='<%#Eval("M_Option_Id") %>'>Active</asp:LinkButton>
                                                                    </div>
                                                                    <div class="plan-left-btn">

                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Modify" CssClass="coursebtn course-btn2" CommandArgument='<%#Eval("M_Option_Id") %>'>Edit</asp:LinkButton>
                                                                    </div>

                                                                </td>
                                                            </tr>

                                                        </ItemTemplate>
                                                    </asp:ListView>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</asp:Content>
