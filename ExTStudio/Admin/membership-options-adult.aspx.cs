﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class membership_options_adult : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.QueryString["MOptionId"] != null)
                        {
                            BindData();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        public void BindData()
        {
            try
            {
                objvalue.M_Option_Id = Convert.ToInt64(Request.QueryString["MOptionId"].ToString());
                ds = objInteraction.Membership_Options_Select_By_Id(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtPackageName.Text = ds.Tables[0].Rows[0]["Package_Name"].ToString();
                        txtPackageTime.Text = ds.Tables[0].Rows[0]["Package_Time"].ToString();
                        txtStarter.Text = ds.Tables[0].Rows[0]["Starter_Price"].ToString();
                        txtTimesPerWeek.Text = ds.Tables[0].Rows[0]["Times_Per_Week"].ToString();
                        txtMonthlyInvestment.Text = ds.Tables[0].Rows[0]["Monthly_Investment"].ToString();
                        hdnId.Value = ds.Tables[0].Rows[0]["M_Option_Id"].ToString();
                        lbtnNext.Text = "Update";
                    }

                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbtnNext.Text == "Next")
                {
                    msgsuccess.Style.Add("display", "none");
                    msgerror.Style.Add("display", "none");

                    objvalue.Package_Name = txtPackageName.Text;
                    objvalue.Package_Time = txtPackageTime.Text;
                    objvalue.Times_Per_Week = txtTimesPerWeek.Text;
                    objvalue.Monthly_Investment = Convert.ToDecimal(txtMonthlyInvestment.Text);
                    objvalue.Is_Kide_Adult = "A";
                    objvalue.Starter_Price = Convert.ToDecimal(txtStarter.Text);
                    objvalue.Flag = "A";
                    objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Membership_Options_Insert(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    String[] optionid = rowEffectedarr[1].ToString().Split(';');

                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();

                        Response.Redirect("~/Admin/membership-product-add.aspx?MOptionId=" + optionid[1]);

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Registration Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (lbtnNext.Text == "Update")
                {
                    msgsuccess.Style.Add("display", "none");
                    msgerror.Style.Add("display", "none");

                    objvalue.Package_Name = txtPackageName.Text;
                    objvalue.Package_Time = txtPackageTime.Text;
                    objvalue.Times_Per_Week = txtTimesPerWeek.Text;
                    objvalue.Monthly_Investment = Convert.ToDecimal(txtMonthlyInvestment.Text);
                    objvalue.Starter_Price = Convert.ToDecimal(txtStarter.Text);
                    objvalue.Is_Kide_Adult = "A";
                    objvalue.M_Option_Id = Convert.ToInt64(hdnId.Value);
                    objvalue.Flag = "A";
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Membership_Options_Update(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    String[] optionid = rowEffectedarr[1].ToString().Split(';');
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();


                        Response.Redirect("~/Admin/membership-options-adult-list.aspx");
                        // Response.Redirect("~/Admin/membership-options.aspx");

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Registration Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtPackageName.Text = txtPackageTime.Text = txtTimesPerWeek.Text = txtMonthlyInvestment.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}