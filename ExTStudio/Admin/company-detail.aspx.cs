﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ExTStudio.Admin
{
    public partial class company_detail : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        public void BindData()
        {
            try
            {
                ds = objInteraction.Company_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        hnId.Value = ds.Tables[0].Rows[0]["Company_Id"].ToString();
                        txtCompanyName.Text = ds.Tables[0].Rows[0]["Company_Name"].ToString();
                        txtCompanyShortName.Text = ds.Tables[0].Rows[0]["Company_Short_Name"].ToString();
                        txtWebsite.Text = ds.Tables[0].Rows[0]["Company_Website"].ToString();
                        txtEmailId.Text = ds.Tables[0].Rows[0]["Company_Email"].ToString();
                        txtOffice.Text = ds.Tables[0].Rows[0]["Company_PhoneNo1"].ToString();
                        txtMobile.Text = ds.Tables[0].Rows[0]["Company_PhoneNo2"].ToString();
                        txtAddressLine1.Text = ds.Tables[0].Rows[0]["Company_Address_Line1"].ToString();
                        txtAddressLine2.Text = ds.Tables[0].Rows[0]["Company_Address_Line2"].ToString();
                        txtAddressLine3.Text = ds.Tables[0].Rows[0]["Company_Street"].ToString();
                        txtAddressLine4.Text = ds.Tables[0].Rows[0]["Company_Nearby"].ToString();
                        txtAddressLine5.Text = ds.Tables[0].Rows[0]["Company_Area"].ToString();
                        txtCity.Text = ds.Tables[0].Rows[0]["Company_City"].ToString();
                        txtState.Text = ds.Tables[0].Rows[0]["Company_State"].ToString();
                        txtCountry.Text = ds.Tables[0].Rows[0]["Company_Country"].ToString();
                        txtPincode.Text = ds.Tables[0].Rows[0]["Company_Pincode"].ToString();
                        hnImagePath.Value = ds.Tables[0].Rows[0]["Company_Logo"].ToString();
                        imgLogoImage.Src = ds.Tables[0].Rows[0]["Company_Logo"].ToString();

                        lbtnSave.Text = "Update";
                    }
                    else
                    {
                        //  txtHost.Text = txtPort.Text = txtUserName.Text = txtPassword.Text = txtEmailType.Text = "";
                        //  hfId.Value = "";
                        //  chkSsl.Checked = false;
                        lbtnSave.Text = "Save";
                    }

                }
                else
                {
                    //  txtHost.Text = txtPort.Text = txtUserName.Text = txtPassword.Text = txtEmailType.Text = "";
                    //   chkSsl.Checked = false;
                    //  hfId.Value = "";
                    lbtnSave.Text = "Save";
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (lbtnSave.Text == "Save")
                {
                    #region CompanyProfile
                    string ProfileImage;
                    if (fuLogo.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuLogo.FileName);
                        string AddressPath = Server.MapPath("~/images/Company/");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("~/images/Company/" + ProfileImage1);
                        ProfileImage = "~/images/Company/" + ProfileImage1;
                        fuLogo.SaveAs(Server.MapPath(ProfileImage));
                    }
                    else
                    {
                        ProfileImage = "~/images/no-image.png";

                    }
                    ProfileImage = ProfileImage.ToString().Replace("~", "..");
                    #endregion

                    objvalue.Company_Name = txtCompanyName.Text.Trim();
                    objvalue.Company_Short_Name = txtCompanyShortName.Text.Trim();
                    objvalue.Company_Address_Line1 = txtAddressLine1.Text.Trim();
                    objvalue.Company_Address_Line2 = txtAddressLine2.Text.Trim();
                    objvalue.Company_Street = txtAddressLine3.Text.Trim();
                    objvalue.Company_Nearby = txtAddressLine4.Text.Trim();
                    objvalue.Company_Area = txtAddressLine5.Text.Trim();
                    objvalue.Company_City = txtCity.Text.Trim();
                    objvalue.Company_State = txtState.Text.Trim();
                    objvalue.Company_Country = txtCountry.Text.Trim();
                    objvalue.Company_Pincode = Convert.ToDecimal(txtPincode.Text == "" ? "0" : txtPincode.Text);
                    objvalue.Company_Email = txtEmailId.Text.Trim();
                    objvalue.Company_PhoneNo1 = Convert.ToDecimal(txtOffice.Text == "" ? "0" : txtOffice.Text);
                    objvalue.Company_PhoneNo2 = Convert.ToDecimal(txtMobile.Text == "" ? "0" : txtMobile.Text);
                    objvalue.Company_Website = txtWebsite.Text.Trim();
                    objvalue.Company_Logo = ProfileImage.ToString();
                    objvalue.Flag = "A";
                    objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Company_Insert(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (lbtnSave.Text == "Update")
                {

                    #region Logo Image Update
                    string ProfileImage;
                    if (fuLogo.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuLogo.FileName);
                        string AddressPath = Server.MapPath("~/images/Company/");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("~/images/Company/" + ProfileImage1);
                        ProfileImage = "~/images/Company/" + ProfileImage1;
                        fuLogo.SaveAs(Server.MapPath(ProfileImage));
                        ProfileImage = ProfileImage.ToString().Replace("~", "..");

                    }
                    else
                    {
                        ProfileImage = hnImagePath.Value;
                    }
                    #endregion

                    objvalue.Company_Name = txtCompanyName.Text.Trim();
                    objvalue.Company_Short_Name = txtCompanyShortName.Text.Trim();
                    objvalue.Company_Address_Line1 = txtAddressLine1.Text.Trim();
                    objvalue.Company_Address_Line2 = txtAddressLine2.Text.Trim();
                    objvalue.Company_Street = txtAddressLine3.Text.Trim();
                    objvalue.Company_Nearby = txtAddressLine4.Text.Trim();
                    objvalue.Company_Area = txtAddressLine5.Text.Trim();
                    objvalue.Company_City = txtCity.Text.Trim();
                    objvalue.Company_State = txtState.Text.Trim();
                    objvalue.Company_Country = txtCountry.Text.Trim();
                    objvalue.Company_Pincode = Convert.ToDecimal(txtPincode.Text == "" ? "0" : txtPincode.Text);
                    objvalue.Company_Email = txtEmailId.Text.Trim();
                    objvalue.Company_PhoneNo1 = Convert.ToDecimal(txtOffice.Text == "" ? "0" : txtOffice.Text);
                    objvalue.Company_PhoneNo2 = Convert.ToDecimal(txtMobile.Text == "" ? "0" : txtMobile.Text);
                    objvalue.Company_Website = txtWebsite.Text.Trim();
                    objvalue.Company_Logo = ProfileImage.ToString();
                    objvalue.Company_Id = Convert.ToInt64(hnId.Value);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Company_Update(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Updated.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
    }
}