﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class products : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }



        #region Bind Data
        public void BindData()
        {
            try
            {
                ds = objInteraction.Product_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        repProduct.DataSource = ds.Tables[0];
                        repProduct.DataBind();
                    }
                    else
                    {
                        repProduct.DataSource = null;
                        repProduct.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                #region File Upload
                String UploadFile;
                if (fuPhoto.HasFile)
                {
                    string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                    string AddressPath = Server.MapPath("~/images/Product");
                    if (!Directory.Exists(AddressPath))
                    {
                        Directory.CreateDirectory(AddressPath);
                    }
                    AddressPath = Server.MapPath("~/images/Product/" + ProfileImage1);
                    UploadFile = "~/images/Product/" + ProfileImage1;
                    fuPhoto.SaveAs(Server.MapPath(UploadFile));
                    UploadFile = UploadFile.ToString().Replace("~", "..");
                    objvalue.ImagePath = UploadFile;
                }
                else
                {
                    objvalue.ImagePath = "../images/no-image.png";
                }

                #endregion

                objvalue.Product_Name = txtProductName.Text;
                objvalue.Size = txtSize.Text;
                objvalue.Price = Convert.ToDecimal(txtPrice.Text);

                objvalue.Flag = "A";
                objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                rowEffectedarr = objInteraction.Product_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    ClearControl();
                    BindData();
                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtProductName.Text = txtSize.Text = txtPrice.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}