﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="reminder-message.aspx.cs" Inherits="ExTStudio.Admin.reminder_message" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Reminder Message</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NAME</th>
                                            <th>Joining Fee</th>
                                            <th>Monthly Bookings</th>
                                            <th>IBAN</th>
                                            <th>TOTAL</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:ListView ID="repBooking" runat="server" OnItemCommand="repBooking_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Full_Name")%></td>
                                                    <td><%# Eval("Joining_Fee")%></td>
                                                    <td><%# Eval("Monthly_Booking")%></td>
                                                    <td><%# Eval("IBAN")%></td>
                                                    <td><%# Eval("Total")%></td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Inactive" CssClass="monthly1 monthly2" CommandArgument='<%#Eval("M_Booking_Id") %>'>X</asp:LinkButton>
                                                        <%--<button class="monthly1 monthly2">Green</button></td>--%>
                                                </tr>

                                            </ItemTemplate>
                                        </asp:ListView>
                                    </tbody>

                                </table>
                            </div>

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">

                                <div class="row">
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                                <label>Send First e-mail</label>
                                                <%-- <button class="email1 email2">Send</button>--%>
                                                <asp:LinkButton runat="server" ID="lbtnDownload" class="btn btn-success" Text="Send" OnClick="lbtnDownload_Click"></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                                <label>Send Second e-mail</label>
                                                <%--<button class="email1 email2">Send</button>--%>
                                                <asp:LinkButton runat="server" ID="LinkButton2" class="btn btn-success" Text="Send" OnClick="lbtnDownload_Click"></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                                <label>Send Final e-mail</label>
                                                <%-- <button class="email1 email2">Send</button>--%>
                                                <asp:LinkButton runat="server" ID="LinkButton3" class="btn btn-success" Text="Send" OnClick="lbtnDownload_Click"></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!--/row-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />--%>
    </div>
</asp:Content>
