﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class create_new_account : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtSName.Text = txtSAddress.Text = txtZip.Text = txtCountry.Text = txtEmailId.Text = txtMobileNo.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                //Random generator = new Random();
                //String Password = generator.Next(0, 1000000).ToString("D6");

                objvalue.Shcool_Name = txtSName.Text;
                objvalue.Address = txtSAddress.Text;
                objvalue.Zip_Code = Convert.ToDecimal(txtZip.Text);
                objvalue.Country = txtCountry.Text;
                objvalue.Email_Id = txtEmailId.Text;
                objvalue.MobileNo = Convert.ToDecimal(txtMobileNo.Text);
                objvalue.Password = txtPassword.Text;// Password;
                objvalue.Flag = "A";
                objvalue.CreateUser = 0;

                rowEffectedarr = objInteraction.Registration_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    Response.Redirect("~/Admin/default.aspx");

                    #region Send Sms & Email
                    //objvalue.Email_Id = txtEmailId.Text.Trim();

                    //if (txtEmailId.Text != null)
                    //{
                    //    String Name = txtSName.Text.Trim() ;

                    //    Title = "Your Account Password";
                    //    Subject = "Your Account Password";
                    //    Body = "Dear " + Name + "<br/> Your Password is " + Password.ToString().Trim() + "<br/><br/>Thank you!<br/><br/><b>Studio Team<b/>";
                    //    CC.SendMailNoReplayCustomer(objvalue.Email_Id, Title, Subject, Body, "NoReplay");
                    //}

                    #endregion

                    ClearControl();
                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Registration Successfully.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else if (rowEfected == -1)
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Records Already Exits.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
    }
}