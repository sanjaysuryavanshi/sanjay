﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class open_booking : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        private void BindData()
        {
            try
            {
                ds = objInteraction.Monthly_Bookings_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        repBooking.DataSource = ds.Tables[0];
                        repBooking.DataBind();
                    }
                    else
                    {
                        repBooking.DataSource = null;
                        repBooking.DataBind();
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        txtFinalTotal.Text = ds.Tables[3].Rows[0]["GrandTotal"].ToString();
                    }
                    else
                    {
                        txtFinalTotal.Text = "";
                    }

                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void repBooking_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (e.CommandName == "Inactive")
                {
                    objvalue.M_Booking_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "D";

                    rowEffectedarr = objInteraction.Monthly_Bookings_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Inactivated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }

        }


    }
}