﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class probetraining : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        private void BindData()
        {
            try
            {
                ds = objInteraction.Location_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = ds.Tables[0];
                        ddlLocation.DataTextField = "Location_Name";
                        ddlLocation.DataValueField = "Location_Id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, "Select Location");
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        ddlTrainer.DataSource = ds.Tables[3];
                        ddlTrainer.DataTextField = "Full_Name";
                        ddlTrainer.DataValueField = "Trainer_Id";
                        ddlTrainer.DataBind();
                        ddlTrainer.Items.Insert(0, "Select Trainer");
                    }
                    else
                    {
                        ddlTrainer.DataSource = null;
                        ddlTrainer.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                objvalue.Location_Id = Convert.ToInt64(ddlLocation.SelectedValue);
                objvalue.Trainer_Id = Convert.ToInt64(ddlTrainer.SelectedValue);
                objvalue.Full_Name = txtFName.Text + " " + txtLName.Text;
                objvalue.MobileNo = Convert.ToDecimal(txtMobileNo.Text);
                objvalue.Email_Id = txtEmailId.Text;
                objvalue.Age = Convert.ToInt64(txtAge.Text);
                objvalue.Heard = lblDays.Text;
                objvalue.GoogleFacebook = rblGorFb.SelectedValue;
                objvalue.Flag = "A";
                objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                rowEffectedarr = objInteraction.Probetraining_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    #region Send Sms & Email
                    objvalue.Email_Id = txtEmailId.Text.Trim();

                    if (txtEmailId.Text != null)
                    {
                        String Name = txtLName.Text.Trim();

                        Title = "Studio (Probetraining)";
                        Subject = "Studio (Probetraining)";
                        Body = "Dear " + Name + "<br/> We look forward to meeting you and Your child " + txtFName.Text.ToString().Trim() + " on " + lblDays.Text.TrimEnd(',') + " at " + ddlLocation.SelectedItem + "Please remember to bring Something to drink and sports Clothes. Your Instructor is " + ddlTrainer.SelectedItem + ". Please comea few minutes early to Meet yourtrainer and get aqainted. Your Studio Team.";
                        CC.SendMailNoReplayCustomer(objvalue.Email_Id, Title, Subject, Body, "NoReplay");
                    }

                    #endregion
                    ClearControl();
                    Response.Redirect("~/Admin/messages.aspx");

                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objvalue.Location_Id = ddlLocation.SelectedIndex == 0 ? 0 : Convert.ToInt64(ddlLocation.SelectedValue);
                ds = objInteraction.Location_Select_By_Id(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["Is_Holiday"].ToString() == "D")
                        {
                            lblDays.Text = ds.Tables[0].Rows[0]["T_Day"].ToString();
                        }
                        else
                        {
                            lblDays.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Holiday_Date"].ToString()).ToShortDateString();
                        }
                    }
                    else
                    {
                        lblDays.Text = "";
                    }
                }
                else
                {
                    lblDays.Text = "";
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                ddlLocation.SelectedIndex = 0;
                ddlTrainer.SelectedIndex = 0;
                txtFName.Text = txtLName.Text = txtMobileNo.Text = txtEmailId.Text = txtAge.Text = "";
                rblGorFb.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}