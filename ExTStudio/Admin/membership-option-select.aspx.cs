﻿using BAL;
using CrystalDecisions.CrystalReports.Engine;
using DAL;
using ExTStudio.AllClasses;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class membership_option_select : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds, ds1 = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.QueryString["MOptionId"] != null)
                        {
                            BindData();
                        }
                        else
                        {
                            Response.Redirect("~/Admin/membership.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        public void BindData()
        {
            try
            {
                objvalue.Member_Id = Convert.ToInt32(Request.QueryString["MOptionId"].ToString());
                ds = objInteraction.Membership_Select_By_Id(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        lbtnSave.Text = "Update";

                        rblKA.SelectedValue = ds.Tables[1].Rows[0]["Is_Kide_Adult"].ToString();

                        objvalue.Is_Kide_Adult = rblKA.SelectedValue;
                        ds1 = objInteraction.Membership_Options_Select_By_IsKideAdult(objvalue);
                        if (ds1.Tables.Count > 0)
                        {
                            if (ds1.Tables[0].Rows.Count > 0)
                            {
                                ddlMOption.DataSource = ds1.Tables[0];
                                ddlMOption.DataTextField = "Package_Name";
                                ddlMOption.DataValueField = "M_Option_Id";
                                ddlMOption.DataBind();
                                ddlMOption.Items.Insert(0, "Select Membership Option");
                            }
                            else
                            {
                                ddlMOption.Items.Clear();
                                ddlMOption.DataSource = null;
                                ddlMOption.DataBind();
                                ddlMOption.Items.Insert(0, "Select Membership Option");
                            }

                        }
                        else
                        {
                            ddlMOption.Items.Clear();
                            ddlMOption.DataSource = null;
                            ddlMOption.DataBind();
                            ddlMOption.Items.Insert(0, "Select Membership Option");
                        }

                        ddlMOption.SelectedValue = ds.Tables[1].Rows[0]["M_Option_Id"].ToString();

                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            repProduct.DataSource = ds.Tables[2];
                            repProduct.DataBind();
                        }
                        else
                        {
                            repProduct.DataSource = null;
                            repProduct.DataBind();
                        }

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            lblStarterPrice.Text = ds.Tables[1].Rows[0]["Starter_Price"].ToString();
                            lblPackageTime.Text = ds.Tables[1].Rows[0]["Package_Time"].ToString();
                            lblTimesPerWeek.Text = ds.Tables[1].Rows[0]["Times_Per_Week"].ToString();
                            lblMonthlyInvestment.Text = ds.Tables[1].Rows[0]["Monthly_Investment"].ToString();
                        }
                        else
                        {
                            lblStarterPrice.Text = "";
                            lblPackageTime.Text = "";
                            lblTimesPerWeek.Text = "";
                            lblMonthlyInvestment.Text = "";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                //if (lbtnSave.Text == "Save")
                //{

                //objvalue.Product_Name = txtProductName.Text;
                objvalue.Member_Id = Convert.ToInt64(Request.QueryString["MOptionId"].ToString());
                objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                objvalue.M_Option_Id = Convert.ToInt64(ddlMOption.SelectedValue);

                rowEffectedarr = objInteraction.Membership_Update_Option(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    ClearControl();
                    // BindData();
                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                //}
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                ddlMOption.SelectedIndex = 0;
                repProduct.DataSource = null;
                repProduct.DataBind();
                lbtnSave.Text = "Save";
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void rblKA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objvalue.Is_Kide_Adult = rblKA.SelectedValue;
                ds = objInteraction.Membership_Options_Select_By_IsKideAdult(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlMOption.DataSource = ds.Tables[0];
                        ddlMOption.DataTextField = "Package_Name";
                        ddlMOption.DataValueField = "M_Option_Id";
                        ddlMOption.DataBind();
                        ddlMOption.Items.Insert(0, "Select Membership Option");
                    }
                    else
                    {
                        ddlMOption.Items.Clear();
                        ddlMOption.DataSource = null;
                        ddlMOption.DataBind();
                        ddlMOption.Items.Insert(0, "Select Membership Option");
                    }

                }
                else
                {
                    ddlMOption.Items.Clear();
                    ddlMOption.DataSource = null;
                    ddlMOption.DataBind();
                    ddlMOption.Items.Insert(0, "Select Membership Option");
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void ddlMOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objvalue.M_Option_Id = Convert.ToInt64(ddlMOption.SelectedValue);
                ds = objInteraction.Membership_Product_Select_By_MOptionId(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        repProduct.DataSource = ds.Tables[0];
                        repProduct.DataBind();
                    }
                    else
                    {
                        repProduct.DataSource = null;
                        repProduct.DataBind();
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        lblStarterPrice.Text = ds.Tables[1].Rows[0]["Starter_Price"].ToString();
                        lblPackageTime.Text = ds.Tables[1].Rows[0]["Package_Time"].ToString();
                        lblTimesPerWeek.Text = ds.Tables[1].Rows[0]["Times_Per_Week"].ToString();
                        lblMonthlyInvestment.Text = ds.Tables[1].Rows[0]["Monthly_Investment"].ToString();
                    }
                    else
                    {
                        lblStarterPrice.Text = "";
                        lblPackageTime.Text = "";
                        lblTimesPerWeek.Text = "";
                        lblMonthlyInvestment.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void lbtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                //DataSet dsReport = new DataSet();
                //dsReport = Session["ApprovalRequiredFromList"];
                //DataSet1 myds = new DataSet1();
                //myds.Tables["tblEmployeeProfile"].Merge(ds.Tables[0]);

                CreateAgreementfoPDF();

                //ReportDocument crDoc = new ReportDocument();
                //crDoc.Load(Server.MapPath("~/Reports/crAgreement.rpt"));
                ////crDoc.SetDataSource(myds);
                ////cr1.ReportSource = crDoc;
                //crDoc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "Agreement");

            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }


        #region Create PDF For Agreement
        public void CreateAgreementfoPDF()
        {
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.Append("<table width='100%'>");

                        sb.Append("<tr>");
                        sb.Append("<td><center><b>Agreement</b></center>");
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append("<td><center><b>Independent Contractor Agreement</b></center>");
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append("<td><p>This Agreement between  ________________ (“Studio”) and the undersigned  ____________, is as follows:</p>");
                        sb.Append("</td>");

                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append("<td><p>1. Qualification: Contractor represent that Contractor is qualified as a ______________ with Substantial experience and/or requisite certification ,as more specifically describe in Exhibit  “A”  aatached here to, necessary to provide the agreed to Services as highlighed in Section 2.</p>");
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append("<td><p>2. Services: Contractor agrees to provides _______________________ (The “Service” ) at Contractor's customery rates to members , guest, and others as allows by studio rules. Contractor shall notify Studio in writing prior to any anticipated rate changes, Which Will allow studio to communicate rtae to members, guest and others.Studio will assist Contractor by Scheduling appoitment on behalf of contractor, if contractor so desires and by advertising the services to members, guests and other in studio's newletter and websites, as well supporting other internal communication opportunities that will assist in making member, guest and others aware of Contractor's service. Contractor agrees to cause all persons who utilize Contractor's Service to execute a Release and Indemnification Agreement as set forth in Exhibit “B”.</p>");
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append("<td><p>3. Term and Termination: The Term of this agreement shall commence of the date the last party signs this agreement and shall continue there after on the month to month basis. Either party mat cancel this Agreement without Cause upon fifteen (15) days prior written notice to the other party of such inteded termission. In addition both parties retain the right to immediatel terminate this agreement for cause upon occurrence of the following circumtances: (i)violation by contractor or studio of any of the provisional of this Agreement , (ii) failture the perform the services by Contractor or perform duties of studio in satisfactory.</p>");
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr>");
                        sb.Append("<td><p>Signed Member	</p>");
                        sb.Append("<td><p>Signed Studio	</p>");
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("</table>");

                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                        string pathImage = Server.MapPath("~/PDFs/AgreementPDFs/");
                        if (!Directory.Exists(pathImage))
                        {
                            Directory.CreateDirectory(pathImage);
                        }
                        String File_Name = pathImage + "Agreement.pdf";

                        using (FileStream fs = new FileStream(File_Name, FileMode.Create, FileAccess.Write, FileShare.None))
                        using (PdfWriter writer = PdfWriter.GetInstance(pdfDoc, fs))
                        {
                            pdfDoc.Open();
                            htmlparser.Parse(sr);
                            pdfDoc.Close();
                        }

                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment;filename=Agreement.pdf");
                        Response.TransmitFile(Server.MapPath("~/PDFs/AgreementPDFs/Agreement.pdf"));
                        Response.End();

                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            // Catching iTextSharp.text.DocumentException if any
            catch (DocumentException de)
            {
                throw de;
            }
            // Catching System.IO.IOException if any
            catch (IOException ioe)
            {
                throw ioe;
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}