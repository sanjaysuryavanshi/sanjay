﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class check_in2 : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                        BindLocation();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Location
        private void BindLocation()
        {
            try
            {
                ds = objInteraction.Location_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = ds.Tables[0];
                        ddlLocation.DataTextField = "Location_Name";
                        ddlLocation.DataValueField = "Location_Id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, "Select Location");
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Bind Data
        public void BindData()
        {
            try
            {
                ds = objInteraction.Course_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        repCourse.DataSource = ds.Tables[2];
                        repCourse.DataBind();
                    }
                    else
                    {
                        repCourse.DataSource = null;
                        repCourse.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (lbtnSave.Text == "Save")
                {
                    objvalue.Location_Id = Convert.ToInt64(ddlLocation.SelectedValue);
                    objvalue.Course_Name = txtCourseName.Text;
                    objvalue.Flag = "A";

                    objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Course_Insert(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (lbtnSave.Text == "Update")
                {
                    objvalue.Location_Id = Convert.ToInt64(ddlLocation.SelectedValue);
                    objvalue.Course_Name = txtCourseName.Text;
                    objvalue.Course_Id = Convert.ToInt64(hnId.Value);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Course_Update(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Updated.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtCourseName.Text = "";
                ddlLocation.SelectedIndex = 0;
                lbtnSave.Text = "Save";
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void repCourse_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (e.CommandName == "Inactive")
                {
                    objvalue.Course_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "D";

                    rowEffectedarr = objInteraction.Course_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Inactivated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (e.CommandName == "Active")
                {
                    objvalue.Course_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "A";

                    rowEffectedarr = objInteraction.Course_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Activated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (e.CommandName == "Modify")
                {
                    objvalue.Course_Id = Convert.ToInt32(e.CommandArgument);
                    ds = objInteraction.Course_Select_By_Id(objvalue);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddlLocation.SelectedValue = ds.Tables[0].Rows[0]["Location_Id"].ToString();
                            txtCourseName.Text = ds.Tables[0].Rows[0]["Course_Name"].ToString();
                            hnId.Value = ds.Tables[0].Rows[0]["Course_Id"].ToString();
                            lbtnSave.Text = "Update";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }

        }
    }
}