﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class trainer_profile : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        public void BindData()
        {
            try
            {
                objvalue.Trainer_Id = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                ds = objInteraction.Trainer_Select_By_Id(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        hnId.Value = ds.Tables[0].Rows[0]["Trainer_Id"].ToString();
                        txtName.Text = ds.Tables[0].Rows[0]["Full_Name"].ToString();
                        txtEmailId.Text = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                        txtMobile.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
                        txtAbout.Text = ds.Tables[0].Rows[0]["About"].ToString();
                        hnImagePath.Value = ds.Tables[0].Rows[0]["ImagePath"].ToString();
                        imgLogoImage.Src = ds.Tables[0].Rows[0]["ImagePath"].ToString();

                        lbtnSave.Text = "Update";
                    }
                    else
                    {
                        //  txtHost.Text = txtPort.Text = txtUserName.Text = txtPassword.Text = txtEmailType.Text = "";
                        //  hfId.Value = "";
                        //  chkSsl.Checked = false;
                        lbtnSave.Text = "Save";
                    }

                }
                else
                {
                    //  txtHost.Text = txtPort.Text = txtUserName.Text = txtPassword.Text = txtEmailType.Text = "";
                    //   chkSsl.Checked = false;
                    //  hfId.Value = "";
                    lbtnSave.Text = "Save";
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (lbtnSave.Text == "Update")
                {

                    #region Logo Image Update
                    string ProfileImage;
                    if (fuLogo.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuLogo.FileName);
                        string AddressPath = Server.MapPath("~/images/Trainer/");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("~/images/Trainer/" + ProfileImage1);
                        ProfileImage = "~/images/Trainer/" + ProfileImage1;
                        fuLogo.SaveAs(Server.MapPath(ProfileImage));
                        ProfileImage = ProfileImage.ToString().Replace("~", "..");

                    }
                    else
                    {
                        ProfileImage = hnImagePath.Value;
                    }
                    #endregion

                    objvalue.Full_Name = txtName.Text.Trim();
                    objvalue.Email_Id = txtEmailId.Text.Trim();
                    objvalue.MobileNo = Convert.ToDecimal(txtMobile.Text == "" ? "0" : txtMobile.Text);
                    objvalue.About = txtAbout.Text.Trim();
                    objvalue.ImagePath = ProfileImage.ToString();
                    objvalue.Trainer_Id = Convert.ToInt64(hnId.Value);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Trainer_Update_Profile(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Updated.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
    }
}