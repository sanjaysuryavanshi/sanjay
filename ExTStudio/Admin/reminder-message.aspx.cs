﻿using BAL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DAL;
using ExTStudio.AllClasses;
using ExTStudio.AllDataSet;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ExTStudio.Admin
{
    public partial class reminder_message : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.QueryString["MBookingId"] != null)
                        {
                            BindData();
                        }
                        else
                        {
                            Response.Redirect("~/Admin/open-booking.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        private void BindData()
        {
            try
            {
                objvalue.M_Booking_Id = Convert.ToInt64(Request.QueryString["MBookingId"].ToString());
                ds = objInteraction.Monthly_Bookings_Select_By_Id(objvalue);
                Session["Booking"] = ds;
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        repBooking.DataSource = ds.Tables[0];
                        repBooking.DataBind();
                    }
                    else
                    {
                        repBooking.DataSource = null;
                        repBooking.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                ds = objInteraction.Company_SelectAll();
                DataSet dsReport = new DataSet();
                dsReport = (DataSet)Session["Booking"];
                //DataSet1 myds = new DataSet1();
                //myds.Tables["dtStudent"].Merge(dsReport.Tables[0]);
                //myds.Tables["dtCompany"].Merge(ds.Tables[0]);

                //ReportDocument crDoc = new ReportDocument();
                //crDoc.Load(Server.MapPath("~/Reports/crReminder.rpt"));
                //crDoc.SetDataSource(myds);
                //// CrystalReportViewer1.ReportSource = crDoc;
                //// crDoc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "Reminder Email");

                //System.IO.Stream st = crDoc.ExportToStream(ExportFormatType.PortableDocFormat);

                //PdfDocument document = PdfReader.Open(st);

                //string pathImage = Server.MapPath("~/PDFs/");
                //if (!Directory.Exists(pathImage))
                //{
                //    Directory.CreateDirectory(pathImage);
                //}
                //// Save the document...
                //string fn = "MyPDF" + DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString() + ".pdf";
                //string filename = "~/PDFs/" + fn;
                //document.Save(Server.MapPath(filename));
                String Company_Name = ds.Tables[0].Rows[0]["Company_Name"].ToString();
                String Company_Address = ds.Tables[0].Rows[0]["Address"].ToString();
                String Company_PhoneNo1 = ds.Tables[0].Rows[0]["Company_PhoneNo1"].ToString();

                String Full_Name = dsReport.Tables[0].Rows[0]["Full_Name"].ToString();
                String Booking_Date = dsReport.Tables[0].Rows[0]["Booking_Date"].ToString();
                String Total = dsReport.Tables[0].Rows[0]["Total"].ToString();
                String MobileNo = dsReport.Tables[0].Rows[0]["MobileNo"].ToString();
                String Address = dsReport.Tables[0].Rows[0]["Address"].ToString();
                String EmailId = dsReport.Tables[0].Rows[0]["Email_Id"].ToString();

                CreateReminderMsgPDF(Company_Name, Company_Address, Company_PhoneNo1, Full_Name, Booking_Date, Total, MobileNo, Address, EmailId);

                //  CC.SendMailWithAttachments(dsReport.Tables[0].Rows[0]["Email_Id"].ToString(), "Studio", "Reminder Message", "Reminder Message", "NoReplay", fn);

            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Create PDF For Reminder Msg
        public void CreateReminderMsgPDF(String Company_Name, String Company_Address, String Company_PhoneNo1, String Full_Name, String Booking_Date, String Total, String MobileNo, String Address, String EmailId)
        {
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        StringBuilder sb = new StringBuilder();


                        sb.Append("<P>From,</P>");
                        sb.Append("<P>" + Company_Name + "</P>");
                        sb.Append("<P>" + Company_PhoneNo1 + "</P>");
                        sb.Append("<P>" + Company_Address + "</P>");
                        sb.Append("<br />");

                        sb.Append("<P> Date :" + System.DateTime.Now.ToString("dd/MM/yyyy") + "</P>");

                        sb.Append("<P>To,</P>");
                        sb.Append("<P>" + Full_Name + "</P>");
                        sb.Append("<P>" + MobileNo + "</P>");
                        sb.Append("<P>" + Address + "</P>");
                        sb.Append("<br />");

                        sb.Append("<P>Subject: Reminder of payment</P>");
                        sb.Append("<br />");

                        sb.Append("<P>Dear Mr./Ms " + Full_Name + "</P>");
                        sb.Append("<br />");

                        sb.Append("<P>This letter is being written to inform you that your payment of (Joining Fee + Monthly Bookings) " + Total + "  has been outstanding for the past " + Booking_Date + " .</P>");
                        sb.Append("<br />");

                        sb.Append("<P>We kindly request you to make the payment, as it is necessary for us to have all our payments in on time.</P>");
                        sb.Append("<br />");

                        sb.Append("<P>We’re sorry for any inconvenience caused.</P>");
                        sb.Append("<br />");

                        sb.Append("<P>Yours sincerely, </P>");
                        sb.Append("<br />");
                        sb.Append("<P>_________________</P>");
                        sb.Append("<br />");
                        sb.Append("<P>" + Company_Name + "</P>");

                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                        string pathImage = Server.MapPath("~/PDFs/ReminderMsgPDFs/");
                        if (!Directory.Exists(pathImage))
                        {
                            Directory.CreateDirectory(pathImage);
                        }
                        string ss = DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString();
                        String File_Name = pathImage + Full_Name + "_ReminderMsg_" + ss + ".pdf";

                        using (FileStream fs = new FileStream(File_Name, FileMode.Create, FileAccess.Write, FileShare.None))
                        using (PdfWriter writer = PdfWriter.GetInstance(pdfDoc, fs))
                        {
                            pdfDoc.Open();
                            htmlparser.Parse(sr);
                            pdfDoc.Close();
                        }

                        string fn = Full_Name + "_ReminderMsg_" + ss + ".pdf";

                        CC.SendMailWithAttachments(EmailId.ToString(), "Studio", "Reminder Message", "Reminder Message", "NoReplay", fn);

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Send Reminder Massage Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

                        //Response.ContentType = "application/pdf";
                        //Response.AppendHeader("Content-Disposition", "attachment;filename=ReminderMsg.pdf");
                        //Response.TransmitFile(Server.MapPath("~/PDFs/ReminderMsgPDFs/" + Full_Name + "_ReminderMsg_" + ss + ".pdf"));
                        //Response.End();

                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            // Catching iTextSharp.text.DocumentException if any
            catch (DocumentException de)
            {
                throw de;
            }
            // Catching System.IO.IOException if any
            catch (IOException ioe)
            {
                throw ioe;
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        protected void repBooking_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (e.CommandName == "Inactive")
                {
                    objvalue.M_Booking_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "D";

                    rowEffectedarr = objInteraction.Monthly_Bookings_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Inactivated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }

        }

    }
}