﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class trainers : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                #region File Upload
                String UploadFile;
                if (fuPhoto.HasFile)
                {
                    string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                    string AddressPath = Server.MapPath("~/images/Trainer");
                    if (!Directory.Exists(AddressPath))
                    {
                        Directory.CreateDirectory(AddressPath);
                    }
                    AddressPath = Server.MapPath("~/images/Trainer/" + ProfileImage1);
                    UploadFile = "~/images/Trainer/" + ProfileImage1;
                    fuPhoto.SaveAs(Server.MapPath(UploadFile));
                    UploadFile = UploadFile.ToString().Replace("~", "..");
                    objvalue.ImagePath = UploadFile;
                }
                else
                {
                    objvalue.ImagePath = "../images/no-image.png";
                }

                #endregion

                objvalue.Full_Name = txtName.Text;
                objvalue.Email_Id = txtEmailId.Text;
                objvalue.See = txtSee.Text;
                objvalue.Do = txtDo.Text;
                objvalue.Admin_Id = 1;
                objvalue.MobileNo = Convert.ToDecimal(txtPhoneNo.Text);
                objvalue.Password = txtPassword.Text;
                objvalue.About = txtAbout.Text;
                objvalue.Flag = "A";
                objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                rowEffectedarr = objInteraction.Trainer_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    Response.Redirect("~/Admin/membership-options.aspx");
                    ClearControl();
                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtName.Text = txtEmailId.Text = txtSee.Text = txtDo.Text = txtPhoneNo.Text = txtPassword.Text = txtAbout.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}