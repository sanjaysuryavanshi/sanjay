﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class master_page : System.Web.UI.MasterPage
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.Cookies["Role"].Value == "A")
                        {
                            AmyProfile.Visible = true;
                            TmyProfile.Visible = false;

                            AChangePsw.Visible = true;
                            TChangePsw.Visible = false;

                            BindData();
                        }
                        else
                        {
                            AmyProfile.Visible = false;
                            TmyProfile.Visible = true;

                            AChangePsw.Visible = false;
                            TChangePsw.Visible = true;

                            BindDataTrainer();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }


        #endregion

        #region Bind Data Trainer
        public void BindDataTrainer()
        {
            try
            {
                objvalue.Trainer_Id = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                ds = objInteraction.Trainer_Select_By_Id(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblName.Text = ds.Tables[0].Rows[0]["Full_Name"].ToString();
                        imgPrfile.ImageUrl = ds.Tables[0].Rows[0]["ImagePath"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Bind Data
        public void BindData()
        {
            try
            {
                objvalue.Admin_Login_Id = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                ds = objInteraction.Admin_Select_By_Id(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblName.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                        imgPrfile.ImageUrl = "../images/favicon.png";
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion


        #region Button LogOut Click
        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Cookies["Role"].Expires = DateTime.Now.AddHours(-1);
                Response.Cookies["AdminLoginId"].Expires = DateTime.Now.AddHours(-1);
                Response.Redirect("~/Admin/login.aspx");
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }

        }
        #endregion
    }
}