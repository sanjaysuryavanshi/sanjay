﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class location : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        //objvalue.Location_Id = 6;
                        //ds = objInteraction.Location_Select_By_Id(objvalue);
                        //string[] ss = ds.Tables[0].Rows[0]["T_Day"].ToString().Split(',');
                        //for (int i = 0; i < chkDays.Items.Count; i++)
                        //{
                        //    string ssss = chkDays.Items[i].Text;
                        //    for (int ii = 0; ii < ss.Length; ii++)
                        //    {
                        //        if (ss[ii] == ssss)
                        //        {
                        //            chkDays.Items[i].Selected = true;
                        //        }
                        //    }
                        //}

                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                #region File Upload
                String UploadFile;
                if (fuPhoto.HasFile)
                {
                    string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                    string AddressPath = Server.MapPath("~/images/Location");
                    if (!Directory.Exists(AddressPath))
                    {
                        Directory.CreateDirectory(AddressPath);
                    }
                    AddressPath = Server.MapPath("~/images/Location/" + ProfileImage1);
                    UploadFile = "~/images/Location/" + ProfileImage1;
                    fuPhoto.SaveAs(Server.MapPath(UploadFile));
                    UploadFile = UploadFile.ToString().Replace("~", "..");
                    objvalue.ImagePath = UploadFile;
                }
                else
                {
                    objvalue.ImagePath = "../images/no-image.png";
                }

                #endregion

                objvalue.T_Day = hdnDays.Value.TrimEnd(',');
                objvalue.Is_Holiday = chkHoliday.Checked == true ? "A" : "D";
                objvalue.Holiday_Date = chkHoliday.Checked == true ? Convert.ToDateTime(txtHolidayDate.Text) : System.DateTime.Now;

                objvalue.Location_Name = txtLocationName.Text;
                objvalue.Address = txtAddress.Text;
                objvalue.MobileNo = Convert.ToDecimal(txtMobileNo.Text);

                objvalue.Flag = "A";
                objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                rowEffectedarr = objInteraction.Location_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    Response.Redirect("~/Admin/trainers.aspx");
                    ClearControl();
                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtLocationName.Text = txtAddress.Text = txtMobileNo.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void ddlchkDays_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                chkHoliday.Style.Add("display", "block");
                txtHolidayDate.Style.Add("display", "none");
                string name = "";

                for (int i = 0; i < chkDays.Items.Count; i++)
                {
                    if (chkDays.Items[i].Selected)
                    {
                        name += chkDays.Items[i].Text + ",";
                        chkHoliday.Style.Add("display", "none");
                        txtHolidayDate.Style.Add("display", "none");
                    }
                }
                hdnDays.Value = name;
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void chkHoliday_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkHoliday.Checked == true)
                {
                    txtHolidayDate.Style.Add("display", "block");

                }
                else
                {
                    txtHolidayDate.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

    }
}