﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="training-timings.aspx.cs" Inherits="ExTStudio.Admin.training_timings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%-- <script>
        function myFunction(FileId) {
            // alert(FileId);
            document.getElementById("myDIV").style.display = "block";
            document.getElementById('<%=hdnRideId.ClientID%>').value = FileId;
        }
    </script>--%>


    <script type="text/javascript">


        function validateForm() {


            if (document.getElementById('<%= chkHoliday.ClientID  %>').checked) {
            }
            else {
                var isAnyCheckBoxChecked = false;
                var checkBoxes = document.getElementById('<%=chkDays.ClientID%>').getElementsByTagName("input");

                for (var i = 0; i < checkBoxes.length; i++) {
                    if (checkBoxes[i].type == "checkbox") {
                        if (checkBoxes[i].checked) {
                            isAnyCheckBoxChecked = true;
                            // alert("Atleast one CheckBox is checked");
                            break;
                        }
                    }
                }

                if (!isAnyCheckBoxChecked) {
                    //alert("No CheckBox is Checked.");
                    document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                    document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select day.';
                    document.getElementById('<%=chkDays.ClientID%>').focus();
                    HideLabel();
                    return isAnyCheckBoxChecked;
                }
            }
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var Form = document.getElementById('<%=txtForm.ClientID%>').value;
            var To = document.getElementById('<%=txtTo.ClientID%>').value;
            var AddOnce = document.getElementById('<%=txtAddOnce.ClientID%>').value;
            var EveryWeek = document.getElementById('<%=txtEveryWeek.ClientID%>').value;

           <%-- if (CourseName == "" || CourseName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter course name.';
                document.getElementById('<%=txtCourseName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else--%>


            if (document.getElementById('<%=ddlLocation.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select location.';
                document.getElementById('<%=ddlLocation.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }

            else if (document.getElementById('<%=ddlCourse.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select course.';
                document.getElementById('<%=ddlCourse.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }

            else if (Form == "" || Form == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter from.';
                document.getElementById('<%=txtForm.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (To == "" || To == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter to.';
                document.getElementById('<%=txtTo.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (AddOnce == "" || AddOnce == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter add once.';
                document.getElementById('<%=txtAddOnce.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (EveryWeek == "" || EveryWeek == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter every week.';
                document.getElementById('<%=txtEveryWeek.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (document.getElementById('<%=ddlTrainer.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select trainer.';
                document.getElementById('<%=ddlTrainer.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }
          <%--  else if (Trainer == "" || Trainer == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter trainer.';
                document.getElementById('<%=txtTrainer.ClientID%>').focus();
                HideLabel();
                return false;
            }--%>

            return true;
        }
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Training Timings</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="traning-white-box">
                <div class="form-group">
                    <label style="padding-left: 15px;">Location</label>
                    <div class="col-md-4 ">
                        <asp:DropDownList runat="server" ID="ddlLocation" class="form-control custom-select">
                        </asp:DropDownList>
                    </div>
                </div>

                <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                    <ProgressTemplate>
                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                            <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

                <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                    <Triggers>
                    </Triggers>
                    <ContentTemplate>

                        <div class="row">
                            <%--<div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>MON</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>TUES</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>WED</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>THURS</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>FRI</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>SAT</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>SUN</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner box-name">

                                        <a href="admin.aspx"><span>HOLIDAY</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                            <div class="col-lg-12">
                                <%-- <div class="card card-inverse card-success">--%>
                                <div class="card-body">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner box-name">

                                            <asp:CheckBoxList runat="server" ID="chkDays" OnSelectedIndexChanged="ddlchkDays_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="MON" Value="MON"></asp:ListItem>
                                                <asp:ListItem Text="TUES" Value="TUES"></asp:ListItem>
                                                <asp:ListItem Text="WED" Value="WED"></asp:ListItem>
                                                <asp:ListItem Text="THURS" Value="THURS"></asp:ListItem>
                                                <asp:ListItem Text="FRI" Value="FRI"></asp:ListItem>
                                                <asp:ListItem Text="SAT" Value="SAT"></asp:ListItem>
                                                <asp:ListItem Text="SUN" Value="SUN"></asp:ListItem>
                                            </asp:CheckBoxList>

                                            <asp:HiddenField ID="hdnDays" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </div>

                            <div class="col-lg-3">
                                <%-- <div class="card card-inverse card-success">--%>
                                <div class="card-body">
                                    <div class="carousel slide" data-ride="carousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner box-name">
                                            <asp:CheckBox runat="server" ID="chkHoliday" Text="Holiday" OnCheckedChanged="chkHoliday_CheckedChanged" AutoPostBack="true" Style="color: grey; display: block;"></asp:CheckBox>
                                            <asp:TextBox runat="server" TextMode="Date" ID="txtHolidayDate" class="form-control" Style="display: none"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="row">
                <%-- <div class="col-lg-3">
                        <asp:HiddenField runat="server" ID="hdnRideId" />
                        <div runat="server" onclick="return myFunction(15)">
                            <div class="training-box">
                                <a href="#">
                                    <span>15:00-16:00<br />
                                        kinder Karate<br />
                                        Helmut
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div runat="server" onclick="return myFunction(16)">
                            <div class="training-box">
                                <a href="#">
                                    <span>16:00-17:00<br />
                                        Self Defence<br />
                                        Helmut
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div runat="server" onclick="return myFunction(17)">
                            <div class="training-box">
                                <a href="#">
                                    <span>17:00-18:00<br />
                                        kinder Karate<br />
                                        Helmut
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>--%>
                <div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-outline-info" id="myDIV" style="display: block">
                                    <div class="card-body">
                                        <div role="form">
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="form-group">
                                                        <label>Course Name</label>
                                                        <%--<input type="text" class="form-control">--%>
                                                        <%-- <asp:TextBox ID="txtCourseName" class="form-control" runat="server" placeholder="Enter Course Name"></asp:TextBox>--%>
                                                        <asp:DropDownList runat="server" ID="ddlCourse" class="form-control custom-select">
                                                        </asp:DropDownList>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>From</label>
                                                        <%--<input type="text" class="form-control">--%>
                                                        <asp:TextBox ID="txtForm" class="form-control" runat="server" placeholder="Enter From" TextMode="Time"></asp:TextBox>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>To</label>
                                                        <%--<input type="text" class="form-control">--%>
                                                        <asp:TextBox ID="txtTo" class="form-control" runat="server" placeholder="Enter To" TextMode="Time"></asp:TextBox>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Add Once</label>
                                                    <%--<input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtAddOnce" class="form-control" runat="server" placeholder="Enter Add Once"></asp:TextBox>

                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Every Week</label>
                                                    <%--<input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtEveryWeek" class="form-control" runat="server" placeholder="Enter Every Week"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="form-group">
                                            <label>Trainer</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <%-- <asp:TextBox ID="txtTrainer" class="form-control" runat="server" placeholder="Enter Trainer"></asp:TextBox>--%>
                                            <asp:DropDownList runat="server" ID="ddlTrainer" class="form-control custom-select">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>


                    </div>

                    <div class="row no-margin">
                        <div class="col-md-12">
                            <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-12 trainig-next">
                    <%--<a href="location.aspx" class="next">Next</a>--%>
                    <asp:LinkButton runat="server" ID="lbtnNext" class="next" Text="Next" OnClientClick="return validateForm()" OnClick="lbtnNext_Click"></asp:LinkButton>

                </div>
            </div>

        </div>

    </div>
    </div>
</asp:Content>
