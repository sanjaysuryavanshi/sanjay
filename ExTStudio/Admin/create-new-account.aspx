﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create-new-account.aspx.cs" Inherits="ExTStudio.Admin.create_new_account" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title>Studio-Admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        function AllowAlphabet(sender) {
            if (!sender.value.match(/^[a-zA-Z]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                // toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();
            }
        }
        function AllowAlphabetSpace(sender) {
            if (!sender.value.match(/^[a-zA-Z ._-]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();

            }
        }

        function validateMobileNo(form) {
            var tin = form.value;
            if (!isNumeric(form)) {
                return false;
            }
            else if (!(tin.length == 10)) {

                //  $.gritter.add({ title: 'Error Message', text: 'Please enter 10 digit Mobile No.' });
                //alert("Please enter 10 digit Mobile No.");

                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter 10 digit Mobile No.';
                form.value = "";
                form.focus();
                HideLabel();
                return false;
            }
        return true;
    }

    function validatenumerics(key) {
        var keycode = (key.which) ? key.which : key.keyCode;
        if (keycode > 31 && (keycode < 48 || keycode > 57)) {
            return false;
        }
        else return true;
    }

    function checkEmail(sender) {
        var email = sender.value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            sender.value = "";
            sender.focus();
            //alert('Please Provide Proper Email Address');
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
            HideLabel();

            //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol required");
            return false;
        }
        else {
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
            return true();
        }
    }


    </script>

    <script>
        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var SName = document.getElementById('<%=txtSName.ClientID%>').value;
            var SAddress = document.getElementById('<%=txtSAddress.ClientID%>').value;
            var Zip = document.getElementById('<%=txtZip.ClientID%>').value;
            var Country = document.getElementById('<%=txtCountry.ClientID%>').value;
            var EmailId = document.getElementById('<%=txtEmailId.ClientID%>').value;
            var MobileNo = document.getElementById('<%=txtMobileNo.ClientID%>').value;

            if (SName == "" || SName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter school name.';
                document.getElementById('<%=txtSName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (SAddress == "" || SAddress == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter school address.';
                document.getElementById('<%=txtSAddress.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Zip == "" || Zip == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter zip.';
                document.getElementById('<%=txtZip.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Country == "" || Country == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter country.';
                document.getElementById('<%=txtCountry.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (EmailId == "" || EmailId == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter email id.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (MobileNo == "" || MobileNo == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter mobile no.';
                document.getElementById('<%=txtMobileNo.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="preloader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                </svg>
            </div>

            <section id="wrapper">
                <div class="login-register" style="background-image: url(assets/images/background/login-register.jpg);">
                    <div class="login-box card">
                        <div class="card-body">

                            <div role="form" class="form-horizontal form-material" id="loginform">
                                <h3 class="box-title m-b-20" style="text-align: center; margin:0;">Create New Aaccount</h3>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <%--<input class="form-control" type="text" required="" placeholder="school name">--%>
                                        <asp:TextBox ID="txtSName" class="form-control" runat="server" MaxLength="100" placeholder="Enter School Name"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%-- <input class="form-control" type="text" required="" placeholder="school Adress">--%>
                                        <asp:TextBox ID="txtSAddress" class="form-control" runat="server" placeholder="Enter School Address"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%-- <input class="form-control" type="text" required="" placeholder="Town">--%>
                                        <asp:TextBox ID="txtZip" class="form-control" runat="server" onkeypress="return validatenumerics(event)" MaxLength="6" placeholder="Enter Zip/Town"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%--<input class="form-control" type="text" required="" placeholder="Country">--%>
                                        <asp:TextBox ID="txtCountry" class="form-control" runat="server" placeholder="Enter Country"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%--<input class="form-control" type="text" required="" placeholder="Tel">--%>
                                        <asp:TextBox ID="txtMobileNo" class="form-control" runat="server" onkeypress="return validatenumerics(event)" MaxLength="10" placeholder="Enter Tel"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%-- <input class="form-control" type="text" required="" placeholder="Email">--%>
                                        <asp:TextBox ID="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)" MaxLength="150" placeholder="Enter Email"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%-- <input class="form-control" type="text" required="" placeholder="Email">--%>
                                        <asp:TextBox ID="txtPassword" TextMode="Password" class="form-control" runat="server" MaxLength="50" placeholder="Enter Password"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group text-center m-t-20">
                                    <div class="col-xs-12">
                                        <%--<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Send</button>--%>
                                        <asp:LinkButton runat="server" ID="lbtnSend" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" Text="Save" OnClientClick="return validateForm()" OnClick="lbtnSend_Click"></asp:LinkButton>

                                    </div>
                                </div>

                            </div>

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>

    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
