﻿using BAL;
using CrystalDecisions.CrystalReports.Engine;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class membership : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds, ds1 = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.Cookies["Role"].Value == "A")
                        {
                            BindMember();
                            BindData();
                        }
                        else
                        {
                            BindDataTrainer();
                            ddlTrainer.SelectedIndex = 1;
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Member
        private void BindMember()
        {
            try
            {
                ds = objInteraction.Membership_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        repMember.DataSource = ds.Tables[3];
                        repMember.DataBind();
                    }
                    else
                    {
                        repMember.DataSource = null;
                        repMember.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Bind Data
        private void BindData()
        {
            try
            {
                ds = objInteraction.Training_Timings_SelectData();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ddlTrainer.DataSource = ds.Tables[1];
                        ddlTrainer.DataTextField = "Full_Name";
                        ddlTrainer.DataValueField = "Trainer_Id";
                        ddlTrainer.DataBind();
                        ddlTrainer.Items.Insert(0, "Select Trainer");
                        ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                    }
                    else
                    {
                        ddlTrainer.DataSource = null;
                        ddlTrainer.DataBind();
                    }

                    //if (ds.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlTrainingTimes.DataSource = ds.Tables[3];
                    //    ddlTrainingTimes.DataTextField = "Locations";
                    //    ddlTrainingTimes.DataValueField = "TTime_Id";
                    //    ddlTrainingTimes.DataBind();
                    //    ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                    //}
                    //else
                    //{
                    //    ddlTrainingTimes.DataSource = null;
                    //    ddlTrainingTimes.DataBind();
                    //}
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Bind Data Trainer
        private void BindDataTrainer()
        {
            try
            {
                objvalue.Trainer_Id = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                ds = objInteraction.Training_Timings_SelectData_For_Trainer(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlTrainer.DataSource = ds.Tables[0];
                        ddlTrainer.DataTextField = "Full_Name";
                        ddlTrainer.DataValueField = "Trainer_Id";
                        ddlTrainer.DataBind();
                        ddlTrainer.Items.Insert(0, "Select Trainer");
                    }
                    else
                    {
                        ddlTrainer.DataSource = null;
                        ddlTrainer.DataBind();
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ddlTrainingTimes.DataSource = ds.Tables[1];
                        ddlTrainingTimes.DataTextField = "Locations";
                        ddlTrainingTimes.DataValueField = "TTime_Id";
                        ddlTrainingTimes.DataBind();
                        ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                    }
                    else
                    {
                        ddlTrainingTimes.DataSource = null;
                        ddlTrainingTimes.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                lbtnNext.Text = "Next";
                ddlTrainer.SelectedIndex = 0;
                ddlTrainingTimes.SelectedIndex = 0;
                txtStudentName.Text = txtAddress.Text = txtZip.Text = txtCountry.Text = txtMobileNo.Text = txtIBANS.Text = "";
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void ddlTrainer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objvalue.Trainer_Id = Convert.ToInt64(ddlTrainer.SelectedValue);
                ds = objInteraction.Training_Timings_SelectData_For_Trainer(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ddlTrainingTimes.DataSource = ds.Tables[1];
                        ddlTrainingTimes.DataTextField = "Locations";
                        ddlTrainingTimes.DataValueField = "TTime_Id";
                        ddlTrainingTimes.DataBind();
                        ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                    }
                    else
                    {
                        ddlTrainingTimes.Items.Clear();
                        ddlTrainingTimes.DataSource = null;
                        ddlTrainingTimes.DataBind();
                        ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void repMember_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (e.CommandName == "Inactive")
                {
                    objvalue.Member_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "D";

                    rowEffectedarr = objInteraction.Membership_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Inactivated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (e.CommandName == "Active")
                {
                    objvalue.Member_Id = Convert.ToInt32(e.CommandArgument);
                    objvalue.UpdateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                    objvalue.Flag = "A";

                    rowEffectedarr = objInteraction.Membership_Delete(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        BindData();

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Activated Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Something went wrong. Please try after sometime.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (e.CommandName == "Modify")
                {
                    objvalue.Member_Id = Convert.ToInt32(e.CommandArgument);
                    ds = objInteraction.Membership_Select_By_Id(objvalue);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddlTrainer.SelectedValue = ds.Tables[0].Rows[0]["Trainer_Id"].ToString();

                            objvalue.Trainer_Id = Convert.ToInt64(ddlTrainer.SelectedValue);
                            ds1 = objInteraction.Training_Timings_SelectData_For_Trainer(objvalue);
                            if (ds1.Tables.Count > 0)
                            {
                                if (ds1.Tables[1].Rows.Count > 0)
                                {
                                    ddlTrainingTimes.DataSource = ds1.Tables[1];
                                    ddlTrainingTimes.DataTextField = "Locations";
                                    ddlTrainingTimes.DataValueField = "TTime_Id";
                                    ddlTrainingTimes.DataBind();
                                    ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                                }
                                else
                                {
                                    ddlTrainingTimes.Items.Clear();
                                    ddlTrainingTimes.DataSource = null;
                                    ddlTrainingTimes.DataBind();
                                    ddlTrainingTimes.Items.Insert(0, "Select Training Times");
                                }
                            }

                            ddlTrainingTimes.SelectedValue = ds.Tables[0].Rows[0]["TTime_Id"].ToString();
                            txtStudentName.Text = ds.Tables[0].Rows[0]["Student_Name"].ToString();
                            txtAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                            txtZip.Text = ds.Tables[0].Rows[0]["Zip_Code"].ToString();
                            txtCountry.Text = ds.Tables[0].Rows[0]["Country"].ToString();
                            txtMobileNo.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
                            txtEmailId.Text = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                            txtIBANS.Text = ds.Tables[0].Rows[0]["IBAN"].ToString();
                            hnId.Value = ds.Tables[0].Rows[0]["Member_Id"].ToString();
                            hnImagePath.Value = ds.Tables[0].Rows[0]["ImagePath"].ToString();
                            lbtnNext.Text = "Update";
                        }
                    }
                }

                if (e.CommandName == "ModifyP")
                {
                    //objvalue.Member_Id = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/Admin/membership-option-select.aspx?MOptionId=" + e.CommandArgument);
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                if (lbtnNext.Text == "Next")
                {
                    #region File Upload
                    String UploadFile;
                    if (fuPhoto.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                        string AddressPath = Server.MapPath("../images/Members");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("../images/Members/" + ProfileImage1);
                        UploadFile = "../images/Members/" + ProfileImage1;
                        fuPhoto.SaveAs(Server.MapPath(UploadFile));
                        objvalue.ImagePath = UploadFile;
                    }
                    else
                    {
                        objvalue.ImagePath = "../images/no-image.png";
                    }

                    #endregion

                    objvalue.TTime_Id = Convert.ToInt64(ddlTrainingTimes.SelectedValue);
                    objvalue.Trainer_Id = Convert.ToInt64(ddlTrainer.SelectedValue);
                    objvalue.Student_Name = txtStudentName.Text;
                    objvalue.Address = txtAddress.Text;
                    objvalue.Zip_Code = Convert.ToDecimal(txtZip.Text);
                    objvalue.Country = txtCountry.Text;
                    objvalue.MobileNo = Convert.ToDecimal(txtMobileNo.Text);
                    objvalue.IBAN = txtIBANS.Text;
                    objvalue.Email_Id = txtEmailId.Text;
                    objvalue.Flag = "A";
                    objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Membership_Insert(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    String[] optionid = rowEffectedarr[1].ToString().Split(';');
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();
                        Response.Redirect("~/Admin/membership-option-select.aspx?MOptionId=" + optionid[1]);
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }

                if (lbtnNext.Text == "Update")
                {
                    #region File Upload
                    String UploadFile;
                    if (fuPhoto.HasFile)
                    {
                        string ProfileImage1 = Path.GetFileName(fuPhoto.FileName);
                        string AddressPath = Server.MapPath("../images/Members");
                        if (!Directory.Exists(AddressPath))
                        {
                            Directory.CreateDirectory(AddressPath);
                        }
                        AddressPath = Server.MapPath("../images/Members/" + ProfileImage1);
                        UploadFile = "../images/Members/" + ProfileImage1;
                        fuPhoto.SaveAs(Server.MapPath(UploadFile));
                        objvalue.ImagePath = UploadFile;
                    }
                    else
                    {
                        objvalue.ImagePath = hnImagePath.Value;
                    }

                    #endregion

                    objvalue.TTime_Id = Convert.ToInt64(ddlTrainingTimes.SelectedValue);
                    objvalue.Trainer_Id = Convert.ToInt64(ddlTrainer.SelectedValue);
                    objvalue.Student_Name = txtStudentName.Text;
                    objvalue.Address = txtAddress.Text;
                    objvalue.Zip_Code = Convert.ToDecimal(txtZip.Text);
                    objvalue.Country = txtCountry.Text;
                    objvalue.MobileNo = Convert.ToDecimal(txtMobileNo.Text);
                    objvalue.IBAN = txtIBANS.Text;
                    objvalue.Email_Id = txtEmailId.Text;
                    objvalue.Flag = "A";
                    objvalue.Member_Id = Convert.ToInt64(hnId.Value);
                    objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                    rowEffectedarr = objInteraction.Membership_Update(objvalue).Split(',');
                    rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                    String[] optionid = rowEffectedarr[1].ToString().Split(';');
                    if (rowEfected != 0 && rowEfected != -1)
                    {
                        ClearControl();
                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void dpPagingContactTop_PreRender(object sender, EventArgs e)
        {
            try
            {
                //  DataTable drSearchData = SearchFun();
                BindMember();
                if (ds.Tables[3].Rows.Count > 0)
                {

                    repMember.DataSource = ds.Tables[3];// drSearchData;
                    repMember.DataBind();
                    dpPagingContactTop.Visible = true;
                }
                else
                {
                    repMember.DataSource = null;
                    repMember.DataBind();
                    dpPagingContactTop.Visible = false;
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

    }
}