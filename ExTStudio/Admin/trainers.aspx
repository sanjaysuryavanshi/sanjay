﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="trainers.aspx.cs" Inherits="ExTStudio.Admin.trainers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function AllowAlphabet(sender) {
            if (!sender.value.match(/^[a-zA-Z]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                // toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();
            }
        }
        function AllowAlphabetSpace(sender) {
            if (!sender.value.match(/^[a-zA-Z ._-]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();

            }
        }

        function validateMobileNo(form) {
            var tin = form.value;
            if (!isNumeric(form)) {
                return false;
            }
            else if (!(tin.length == 10)) {

                //  $.gritter.add({ title: 'Error Message', text: 'Please enter 10 digit Mobile No.' });
                //alert("Please enter 10 digit Mobile No.");

                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter 10 digit Mobile No.';
                form.value = "";
                form.focus();
                HideLabel();
                return false;
            }
        return true;
    }

    function validatenumerics(key) {
        var keycode = (key.which) ? key.which : key.keyCode;
        if (keycode > 31 && (keycode < 48 || keycode > 57)) {
            return false;
        }
        else return true;
    }

    function checkEmail(sender) {
        var email = sender.value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            sender.value = "";
            sender.focus();
            //alert('Please Provide Proper Email Address');
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
            HideLabel();

            //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol required");
            return false;
        }
        else {
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
            return true();
        }
    }


    </script>

    <script>

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';


            var Name = document.getElementById('<%=txtName.ClientID%>').value;
            var Email = document.getElementById('<%=txtEmailId.ClientID%>').value;
            var See = document.getElementById('<%=txtSee.ClientID%>').value;
            var Do = document.getElementById('<%=txtDo.ClientID%>').value;
            var PhoneNo = document.getElementById('<%=txtPhoneNo.ClientID%>').value;
            var Password = document.getElementById('<%=txtPassword.ClientID%>').value;
            var About = document.getElementById('<%=txtAbout.ClientID%>').value;


            if (Name == "" || Name == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter name.';
                document.getElementById('<%=txtName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Email == "" || Email == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter email address.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (See == "" || See == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter see.';
                document.getElementById('<%=txtSee.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Do == "" || Do == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter do.';
                document.getElementById('<%=txtDo.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (PhoneNo == "" || PhoneNo == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter phone no.';
                document.getElementById('<%=txtPhoneNo.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Password == "" || Password == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter password.';
                document.getElementById('<%=txtPassword.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (About == "" || About == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter about.';
                document.getElementById('<%=txtAbout.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Trainers</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>


        <div class="col-md-8">
            <div class="row">
                <div class="col-lg-12">
                    <div class=" card-outline-info">

                        <div class="card-body">
                            <div role="form">
                                <div class="form-body">

                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Photo</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:FileUpload ID="fuPhoto" class="form-control ImageFile"
                                                    runat="server"></asp:FileUpload>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtName" class="form-control" runat="server"
                                                    MaxLength="100"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Email Address</label>
                                                <%-- <input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)"
                                                    MaxLength="150"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <h2>Level Of Access</h2>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>See</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtSee" class="form-control" runat="server"
                                                    MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Do</label>
                                                <%-- <input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtDo" class="form-control" runat="server"
                                                    MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Admin</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:DropDownList ID="ddlAdmin" class="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Telephone</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtPhoneNo" class="form-control" runat="server" onkeypress="return validatenumerics(event)"
                                                    MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <%-- <input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtPassword" TextMode="Password" class="form-control" runat="server"
                                                    MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Tell us about this trainer</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtAbout" TextMode="MultiLine" class="form-control" runat="server"
                                                    MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-actions">
                                    <%--<a href="membership-options.aspx" type="submit" class="btn btn-success">Save Location</a>--%>
                                    <asp:LinkButton runat="server" ID="lbtnSave" class="btn btn-success" Text="Save" OnClick="lbtnSave_Click" OnClientClick="return validateForm()"></asp:LinkButton>


                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>


</asp:Content>
