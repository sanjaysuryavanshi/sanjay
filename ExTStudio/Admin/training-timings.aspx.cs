﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class training_timings : System.Web.UI.Page
    {

        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        BindData();
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }

                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Data
        private void BindData()
        {
            try
            {
                ds = objInteraction.Training_Timings_SelectData();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = ds.Tables[0];
                        ddlLocation.DataTextField = "Location_Name";
                        ddlLocation.DataValueField = "Location_Id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, "Select Location");
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ddlTrainer.DataSource = ds.Tables[1];
                        ddlTrainer.DataTextField = "Full_Name";
                        ddlTrainer.DataValueField = "Trainer_Id";
                        ddlTrainer.DataBind();
                        ddlTrainer.Items.Insert(0, "Select Trainer");
                    }
                    else
                    {
                        ddlTrainer.DataSource = null;
                        ddlTrainer.DataBind();
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        ddlCourse.DataSource = ds.Tables[2];
                        ddlCourse.DataTextField = "Course_Name";
                        ddlCourse.DataValueField = "Course_Id";
                        ddlCourse.DataBind();
                        ddlCourse.Items.Insert(0, "Select Course");
                    }
                    else
                    {
                        ddlCourse.DataSource = null;
                        ddlCourse.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtAddOnce.Text = txtEveryWeek.Text = txtForm.Text = txtTo.Text = "";

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void ddlchkDays_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                chkHoliday.Style.Add("display", "block");
                txtHolidayDate.Style.Add("display", "none");
                string name = "";

                for (int i = 0; i < chkDays.Items.Count; i++)
                {
                    if (chkDays.Items[i].Selected)
                    {
                        name += chkDays.Items[i].Text + ",";
                        chkHoliday.Style.Add("display", "none");
                        txtHolidayDate.Style.Add("display", "none");
                    }
                }
                hdnDays.Value = name;
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void chkHoliday_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkHoliday.Checked == true)
                {
                    txtHolidayDate.Style.Add("display", "block");

                }
                else
                {
                    txtHolidayDate.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                //Random generator = new Random();
                //String Password = generator.Next(0, 1000000).ToString("D6");

                //if (hdnRideId.Value == "15")
                //{
                //    objvalue.T_Shif = "15:00 - 16:00 , Kinder Karate";
                //}
                //else if (hdnRideId.Value == "16")
                //{
                //    objvalue.T_Shif = "16:00 - 17:00 , Self Defence";
                //}
                //else if (hdnRideId.Value == "17")
                //{
                //    objvalue.T_Shif = "17:00 - 18:00 , Kinder Karate";
                //}
                // objvalue.T_Shif = hdnRideId.Value;
                objvalue.Location_Id = Convert.ToInt64(ddlLocation.SelectedValue);
                objvalue.T_Day = hdnDays.Value.TrimEnd(',');
                objvalue.Is_Holiday = chkHoliday.Checked == true ? "A" : "D";
                objvalue.Holiday_Date = chkHoliday.Checked == true ? Convert.ToDateTime(txtHolidayDate.Text) : System.DateTime.Now;
                objvalue.Course_Id = Convert.ToInt64(ddlCourse.SelectedValue);
                objvalue.Course_Form = txtForm.Text;
                objvalue.Course_To = txtTo.Text;
                objvalue.AddOnce = txtAddOnce.Text;
                objvalue.EveryWeek = txtEveryWeek.Text;// Password;
                objvalue.Trainer_Id = Convert.ToInt64(ddlTrainer.SelectedValue);
                objvalue.Flag = "A";
                objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                rowEffectedarr = objInteraction.Training_Timings_Insert(objvalue).Split(',');
                rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                if (rowEfected != 0 && rowEfected != -1)
                {
                    ClearControl();

                    Response.Redirect("~/Admin/location.aspx");

                    #region Send Sms & Email
                    //objvalue.Email_Id = txtEmailId.Text.Trim();

                    //if (txtEmailId.Text != null)
                    //{
                    //    String Name = txtSName.Text.Trim() ;

                    //    Title = "Your Account Password";
                    //    Subject = "Your Account Password";
                    //    Body = "Dear " + Name + "<br/> Your Password is " + Password.ToString().Trim() + "<br/><br/>Thank you!<br/><br/><b>Studio Team<b/>";
                    //    CC.SendMailNoReplayCustomer(objvalue.Email_Id, Title, Subject, Body, "NoReplay");
                    //}

                    #endregion


                    msgsuccess.Style.Add("display", "block");
                    lblSuccessMsg.InnerHtml = "Registration Successfully.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }

        }
    }
}