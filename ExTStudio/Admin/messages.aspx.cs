﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class messages : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.Cookies["AdminLoginId"] != null)
                    {
                        if (Request.Cookies["Role"].Value == "A")
                        {
                            BindStudent();
                        }
                        else
                        {
                            BindStudentTrainer();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Admin/login.aspx");
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Bind Student
        private void BindStudent()
        {
            try
            {
                ds = objInteraction.Membership_SelectAll();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        chkMultiple.DataSource = ds.Tables[0];
                        chkMultiple.DataTextField = "Student_Name";
                        chkMultiple.DataValueField = "MobileNo";
                        chkMultiple.DataBind();
                    }
                    else
                    {
                        chkMultiple.DataSource = null;
                        chkMultiple.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        #region Bind Student Trainer
        private void BindStudentTrainer()
        {
            try
            {
                objvalue.Trainer_Id = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);
                ds = objInteraction.Membership_Select_By_TrainerId(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        chkMultiple.DataSource = ds.Tables[0];
                        chkMultiple.DataTextField = "Student_Name";
                        chkMultiple.DataValueField = "MobileNo";
                        chkMultiple.DataBind();
                    }
                    else
                    {
                        chkMultiple.DataSource = null;
                        chkMultiple.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                #region Send Sms To customer

                if (rblSM.SelectedValue == "M")
                {

                    if (hdnMultiMoNo.Value.ToString().Trim() != "")
                    {
                        String BodyDataSMS = txtMessage.Text;
                        String SubjectSMS = "Studio Team";
                        CC.SendSmsTransaction(hdnMultiMoNo.Value, BodyDataSMS);

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Message Send Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
                else
                {
                    if (txtSingle.Text.ToString().Trim() != "")
                    {
                        String BodyDataSMS = txtMessage.Text;
                        String SubjectSMS = "Studio Team";
                        CC.SendSmsTransaction(txtSingle.Text, BodyDataSMS);

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Message Send Successfully.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                }
                #endregion

                //objvalue.Message = txtMessage.Text;
                //objvalue.BVK = txtBVK.Text;
                //objvalue.Users = txtuser.Text;

                //objvalue.Flag = "A";
                //objvalue.CreateUser = Convert.ToInt64(Request.Cookies["AdminLoginId"] == null ? "0" : Request.Cookies["AdminLoginId"].Value);

                //rowEffectedarr = objInteraction.Message_Insert(objvalue).Split(',');
                //rowEfected = Convert.ToInt32(rowEffectedarr[0]);
                //if (rowEfected != 0 && rowEfected != -1)
                //{
                //    ClearControl();

                //    // Response.Redirect("~/Admin/membership-options.aspx");

                //    msgsuccess.Style.Add("display", "block");
                //    lblSuccessMsg.InnerHtml = "Record Successfully Inserted.";
                //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                //}
                //else
                //{
                //    msgerror.Style.Add("display", "block");
                //    lblErrorMsg.InnerHtml = "Opps! Something Went Wrong.";
                //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                //}

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void ddlSM_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rblSM.SelectedValue == "M")
                {
                    chkMultiple.Style.Add("display", "block");
                    txtSingle.Style.Add("display", "none");
                }
                else
                {
                    txtSingle.Style.Add("display", "block");
                    chkMultiple.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void chkMultiple_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string name = "";
                for (int i = 0; i < chkMultiple.Items.Count; i++)
                {
                    if (chkMultiple.Items[i].Selected)
                    {
                        name += chkMultiple.Items[i].Value + ",";
                    }
                }
                hdnMultiMoNo.Value = name.TrimEnd(',');
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        #region Clear Control
        private void ClearControl()
        {
            try
            {
                txtMessage.Text = txtBVK.Text = txtuser.Text = "";
            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
        #endregion
    }
}