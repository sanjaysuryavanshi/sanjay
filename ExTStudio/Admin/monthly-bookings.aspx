﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="monthly-bookings.aspx.cs" Inherits="ExTStudio.Admin.monthly_bookings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function AllowFloat(sender) {
            if (!sender.value.match(/^[0-9.]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warnin<a href="monthly-bookings.aspx">monthly-bookings.aspx</a>g('Please Enter only Decimal in text', 'Invalid Text');
                //alert('Please Enter only Decimal in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only Decimal in text.';
                HideLabel();
            }
        }

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var JF = document.getElementById('<%=txtJoiningFee.ClientID%>').value;
            var MB = document.getElementById('<%=txtMonthlyBookings.ClientID%>').value;
            var IBNS = document.getElementById('<%=txtIBANS.ClientID%>').value;
            var Total = document.getElementById('<%=txtTotal.ClientID%>').value;

            if (document.getElementById('<%=ddlStudent.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select student.';
                document.getElementById('<%=ddlStudent.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }

            else if (JF == "" || JF == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter joining fee.';
                document.getElementById('<%=txtJoiningFee.ClientID%>').focus();
                HideLabel();
                return false;
            }

            else if (MB == "" || MB == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter monthly bookings.';
                document.getElementById('<%=txtMonthlyBookings.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (IBNS == "" || IBNS == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter IBANS.';
                document.getElementById('<%=txtIBANS.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Total == "" || Total == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter total.';
                document.getElementById('<%=txtTotal.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Monthly Bookings</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">

                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <%-- <input type="text" class="form-control">--%>
                                            <asp:DropDownList runat="server" ID="ddlStudent" class="form-control"></asp:DropDownList>

                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Joining Fee</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtJoiningFee" class="form-control" runat="server" onkeypress="return AllowFloat(this)" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Booking Date</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtBDate" TextMode="Date" class="form-control" runat="server" ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                                            <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                                <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtMonthlyBookings" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Monthly Bookings</label>
                                                    <%-- <input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtMonthlyBookings" class="form-control" runat="server" onkeypress="return AllowFloat(this)" MaxLength="10" OnTextChanged="txtMonthlyBookings_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>IBAN</label>
                                                    <asp:TextBox ID="txtIBANS" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                                    <%-- <input type="text" class="form-control">--%>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>TOTAL</label>
                                                    <%-- <input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtTotal" class="form-control" runat="server" MaxLength="10"></asp:TextBox>

                                                </div>
                                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                            <!--/row-->

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>NAME</th>
                                        <th>Joining Fee</th>
                                        <th>Monthly Bookings</th>
                                        <th>IBAN</th>
                                        <th>TOTAL</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:ListView ID="repBooking" runat="server" OnItemCommand="repBooking_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("Full_Name")%></td>
                                                <td><%# Eval("Joining_Fee")%></td>
                                                <td><%# Eval("Monthly_Booking")%></td>
                                                <td><%# Eval("IBAN")%></td>
                                                <td><%# Eval("Total")%></td>
                                                <td>
                                                    <asp:LinkButton ID="LinkButton1" Enabled='<%# Eval("IsPaid").ToString() == "D" ? true : false %>' runat="server" CommandName="Inactive" CssClass="monthly1 monthly2" CommandArgument='<%#Eval("M_Booking_Id") %>'>X</asp:LinkButton>
                                                    <%--<button class="monthly1 monthly2">Green</button></td>--%>
                                            </tr>

                                        </ItemTemplate>
                                    </asp:ListView>
                                    <%-- <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>

                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>--%>
                                </tbody>

                            </table>
                        </div>

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <%--<div class="form-group row">
                                        <div class="col-md-9">
                                           
                                            <asp:LinkButton runat="server" ID="lbtnBookNow" class="next" Text=" Book Now " OnClientClick="return validateForm()" OnClick="lbtnBookNow_Click"></asp:LinkButton>

                                        </div>
                                    </div>--%>
                                </div>
                                <div class="col-md-4 monthly-totl">
                                    <div class="form-group row monthly-totl">
                                        <div class="col-md-9">
                                            <%-- <input type="text" class="form-control" placeholder="Total">--%>
                                            <asp:TextBox ID="txtFinalTotal" class="form-control" runat="server" MaxLength="10"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                     </div>
                                <div class="col-md-4">
                                     </div>
                                 <div class="col-md-3">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                            <%--<button class="monthly1 monthly2">Test Booking</button>--%>
                                            <%-- <button class="monthly1 monthly2">Book Now</button>--%>
                                            <asp:LinkButton runat="server" ID="LinkButton2" class="next" Text=" Book Now " OnClientClick="return validateForm()" OnClick="lbtnBookNow_Click"></asp:LinkButton>

                                        </div>
                                    </div>
                                </div>



                            </div>
                            <!--/row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


</asp:Content>

