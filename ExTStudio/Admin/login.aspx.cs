﻿using BAL;
using DAL;
using ExTStudio.AllClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExTStudio.Admin
{
    public partial class login : System.Web.UI.Page
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet ds = new DataSet();
        String Subject, Body;
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        ComanClass CC = new ComanClass();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void lbtnLogIn_Click(object sender, EventArgs e)
        {
            try
            {
                objvalue.Email_Id = txtEmailId.Text;
                objvalue.Password = txtPassword.Text;

                if (chkTrainer.Checked != true)
                {
                    ds = objInteraction.Admin_Login(objvalue);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            Response.Cookies["AdminLoginId"].Value = ds.Tables[0].Rows[0]["Admin_Login_Id"].ToString().Trim();
                            Response.Cookies["Role"].Value = ds.Tables[0].Rows[0]["Role"].ToString().Trim();
                            Response.Cookies["AdminLoginId"].Expires = DateTime.Now.AddHours(15);
                            Response.Cookies["Role"].Expires = DateTime.Now.AddHours(15);

                            Response.Redirect("~/Admin/default.aspx");
                        }
                        else
                        {
                            //lblMessage.Text = "Please Check Your Login Detail";
                            //lblMessage.ForeColor = System.Drawing.Color.Red;
                            //lblMessage.Visible = true;
                        }
                    }
                }
                else
                {
                    ds = objInteraction.Trainer_Login(objvalue);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            Response.Cookies["AdminLoginId"].Value = ds.Tables[0].Rows[0]["Admin_Login_Id"].ToString().Trim();
                            Response.Cookies["Role"].Value = ds.Tables[0].Rows[0]["Role"].ToString().Trim();
                            Response.Cookies["AdminLoginId"].Expires = DateTime.Now.AddHours(15);
                            Response.Cookies["Role"].Expires = DateTime.Now.AddHours(15);

                            Response.Redirect("~/Admin/check-in.aspx");
                        }
                        else
                        {
                            //lblMessage.Text = "Please Check Your Login Detail";
                            //lblMessage.ForeColor = System.Drawing.Color.Red;
                            //lblMessage.Visible = true;
                        }
                    }
                }
            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }

        protected void lbtnReset_Click(object sender, EventArgs e)
        {
            try
            {
                msgsuccess.Style.Add("display", "none");
                msgerror.Style.Add("display", "none");

                objvalue.Email_Id = txtFEmailId.Text.Trim();
                objvalue.IsTrainer = chkFTrainer.Checked == true ? "A" : "D";

                ds = objInteraction.Admin_Login_By_EmailId(objvalue);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //String ToEmail, String Title, String subject, String Body
                        #region Send Sms & Email
                        objvalue.Email_Id = txtFEmailId.Text.Trim();
                        objvalue.UserName = ds.Tables[0].Rows[0]["UserName"].ToString();
                        objvalue.Password = ds.Tables[0].Rows[0]["Password"].ToString();

                        if (txtEmailId.Text != null)
                        {
                            String Name = objvalue.UserName;

                            Title = "Forget Password";
                            Subject = "Forget Password";
                            Body = "Dear " + Name + "<br/> Your Email Id is " + txtEmailId.Text.Trim() + "<br/> Your Password is " + objvalue.Password + "<br/><br/>Thank you!<br/><br/><b>Studio Team<b/>";
                            CC.SendMailNoReplayCustomer(objvalue.Email_Id, Title, Subject, Body, "NoReplay");

                            txtEmailId.Text = "";
                        }

                        #endregion

                        msgsuccess.Style.Add("display", "block");
                        lblSuccessMsg.InnerHtml = "Password Send Your EmailID.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

                    }
                    else
                    {
                        // Response.Write("<script>alert('Please Enter Valid EmailID')</script>");
                        msgerror.Style.Add("display", "block");
                        lblErrorMsg.InnerHtml = "Please Enter Valid EmailID.";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

                    }
                }
                else
                {
                    msgerror.Style.Add("display", "block");
                    lblErrorMsg.InnerHtml = "Please Enter Valid EmailID.";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

                    // Response.Write("<script>alert('Please Enter Valid EmailID')</script>");
                }

            }
            catch (Exception ex)
            {
                E.ErrorLogInsert("1000", ex.GetType().ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), ex.Source.ToString(), HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            }
        }
    }
}