﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="location.aspx.cs" Inherits="ExTStudio.Admin.location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function validatenumerics(key) {
            var keycode = (key.which) ? key.which : key.keyCode;
            if (keycode > 31 && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else return true;
        }


        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';


            var PName = document.getElementById('<%=txtLocationName.ClientID%>').value;
            var Address = document.getElementById('<%=txtAddress.ClientID%>').value;
            var MobileNo = document.getElementById('<%=txtMobileNo.ClientID%>').value;

            if (PName == "" || PName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter location name.';
                document.getElementById('<%=txtLocationName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Address == "" || Address == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter location address.';
                document.getElementById('<%=txtAddress.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (MobileNo == "" || MobileNo == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter mobile no.';
                document.getElementById('<%=txtMobileNo.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Location</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i>
                </button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="traning-white-box">

                <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                    <ProgressTemplate>
                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                            <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

                <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                    <Triggers>
                    </Triggers>
                    <ContentTemplate>

                        <div class="row">

                            <div class="col-lg-12">
                                <%-- <div class="card card-inverse card-success">--%>
                                <div class="card-body">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner box-name">

                                            <asp:CheckBoxList runat="server" ID="chkDays" OnSelectedIndexChanged="ddlchkDays_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="MON" Value="MON"></asp:ListItem>
                                                <asp:ListItem Text="TUES" Value="TUES"></asp:ListItem>
                                                <asp:ListItem Text="WED" Value="WED"></asp:ListItem>
                                                <asp:ListItem Text="THURS" Value="THURS"></asp:ListItem>
                                                <asp:ListItem Text="FRI" Value="FRI"></asp:ListItem>
                                                <asp:ListItem Text="SAT" Value="SAT"></asp:ListItem>
                                                <asp:ListItem Text="SUN" Value="SUN"></asp:ListItem>
                                            </asp:CheckBoxList>

                                            <asp:HiddenField ID="hdnDays" runat="server" />


                                        </div>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </div>

                            <div class="col-lg-3">
                                <%-- <div class="card card-inverse card-success">--%>
                                <div class="card-body">
                                    <div class="carousel slide" data-ride="carousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner box-name">
                                            <asp:CheckBox runat="server" ID="chkHoliday" Text="Holiday" OnCheckedChanged="chkHoliday_CheckedChanged" AutoPostBack="true" Style="color:grey; display:block;"></asp:CheckBox>

                                            <asp:TextBox runat="server" TextMode="Date" ID="txtHolidayDate" class="form-control" Style="display: none"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>


         
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-outline-info" id="myDIV" style="display: block;">
                            <div class="card-body">
                                <div role="form">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Location photos</label>
                                                <%-- <input type="text" class="form-control">--%>
                                                <asp:FileUpload ID="fuPhoto" class="form-control ImageFile"
                                                    runat="server"></asp:FileUpload>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Location Name</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:TextBox ID="txtLocationName" class="form-control" runat="server"
                                                    MaxLength="100"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtAddress" class="form-control"
                                                runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Telephone </label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox ID="txtMobileNo" class="form-control" runat="server" onkeypress="return validatenumerics(event)"
                                                MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <%--<a href="#" class="btn btn-success">Save Location</a>--%>
                                <asp:LinkButton runat="server" ID="lbtnSave" class="btn btn-success" Text="Save Location" OnClick="lbtnSave_Click" OnClientClick="return validateForm()"></asp:LinkButton>
                            </div>

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>


            </div>

        </div>

    

</asp:Content>
