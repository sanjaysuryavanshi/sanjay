﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="messages.aspx.cs" Inherits="ExTStudio.Admin.messages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var Msg = document.getElementById('<%=txtMessage.ClientID%>').value;
           <%-- var Bvk = document.getElementById('<%=txtBVK.ClientID%>').value;
            var User = document.getElementById('<%=txtuser.ClientID%>').value;--%>

            if (Msg == "" || Msg == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter message.';
                document.getElementById('<%=txtMessage.ClientID%>').focus();
                HideLabel();
                return false;
            }
           <%-- else if (Bvk == "" || Bvk == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter BVK.';
                document.getElementById('<%=txtBVK.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (User == "" || User == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter user.';
                document.getElementById('<%=txtuser.ClientID%>').focus();
                HideLabel();
                return false;
            }--%>

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Message</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>


        <div class="col-md-5 chart-prob">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-outline-info prob-table" id="myDIV" style="display: block;">
                        <div class="card-body">
                            <div role="form">

                                <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                                            <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                                <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="rblSM" />
                                        <asp:AsyncPostBackTrigger ControlID="chkMultiple" />

                                    </Triggers>
                                    <ContentTemplate>


                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <%--<label> Name</label>--%>
                                                    <%--  <input type="text" class="form-control">--%>
                                                    <asp:RadioButtonList runat="server" ID="rblSM" OnSelectedIndexChanged="ddlSM_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="Single" Value="S"></asp:ListItem>
                                                        <asp:ListItem Text="Multiple" Value="M"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:HiddenField runat="server" ID="hdnMultiMoNo" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <%--<label> Name</label>--%>
                                                    <%--  <input type="text" class="form-control">--%>
                                                    <asp:CheckBoxList runat="server" ID="chkMultiple" OnSelectedIndexChanged="chkMultiple_SelectedIndexChanged" AutoPostBack="true" Style="display: none"></asp:CheckBoxList>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <%--<label> Name</label>--%>
                                                    <%--  <input type="text" class="form-control">--%>
                                                    <asp:TextBox runat="server" ID="txtSingle" class="form-control" Style="display: none" placeholder="Enter Mobile No" MaxLength="10"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <%--<label> Name</label>--%>
                                            <%--  <input type="text" class="form-control">--%>
                                            <asp:TextBox runat="server" TextMode="MultiLine" ID="txtMessage" class="form-control" placeholder="Enter Message"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="display: none">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>BVK</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox runat="server" ID="txtBVK" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="display: none">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>USER</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox runat="server" ID="txtuser" class="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>

                            </div>




                        </div>

                        <div class="col-lg-12">
                            <%--  <a href="#" class="next">Next</a>--%>
                            <asp:LinkButton runat="server" ID="lbtnNext" class="next" Text="Send" OnClientClick="return validateForm()" OnClick="lbtnNext_Click"></asp:LinkButton>

                        </div>

                    </div>

                    <div class="row no-margin">
                        <div class="col-md-12">
                            <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>




    </div>
</asp:Content>
