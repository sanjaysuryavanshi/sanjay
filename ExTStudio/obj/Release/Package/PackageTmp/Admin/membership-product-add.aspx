﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="membership-product-add.aspx.cs" Inherits="ExTStudio.Admin.membership_product_add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function validatenumerics(key) {
            var keycode = (key.which) ? key.which : key.keyCode;
            if (keycode > 31 && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else return true;
        }

        function AllowAlphabetSpace(sender) {
            if (!sender.value.match(/^[a-zA-Z ._-]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();

            }
        }

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';


            var PName = document.getElementById('<%=txtProductName.ClientID%>').value;

            if (PName == "" || PName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter product name.';
                document.getElementById('<%=txtProductName.ClientID%>').focus();
                HideLabel();
                return false;
            }

            return true;
        }
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Membership Option Products</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class=" card-outline-info">
                        <div class="card-body">
                            <div role="form" class="form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <%--<input type="text" class="form-control" placeholder="Product Name">--%>
                                                    <asp:TextBox ID="txtProductName" class="form-control" runat="server" onchange="AllowAlphabetSpace(this)" placeholder="Product Name" MaxLength="100"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <div class="col-md-5">
                                            <div class="form-group row">

                                                <div class="col-md-12">
                                                    <%--<input type="text" class="form-control" placeholder="Photo">--%>
                                                    <asp:FileUpload ID="fuPhoto" class="form-control ImageFile" runat="server"></asp:FileUpload>
                                                    <asp:HiddenField runat="server" ID="hdnId" />
                                                    <asp:HiddenField runat="server" ID="hdnMOptionId" />
                                                    <asp:HiddenField runat="server" ID="hdnImahePath" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="col-md-12">
                                        <%-- <a href="#" class="btn btn-success">Orer Now</a>--%>
                                        <asp:LinkButton runat="server" ID="lbtnSave" class="btn btn-success" Text="Save" OnClick="lbtnSave_Click" OnClientClick="return validateForm()"></asp:LinkButton>

                                        <a href="membership-options.aspx" class="next">Next</a>

                                    </div>


                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-12">

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Products Name</th>
                                                <th>Photo</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:ListView ID="repProduct" runat="server" OnItemCommand="repProduct_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("Product_Name")%></td>
                                                        <td>
                                                            <asp:Image runat="server" ImageUrl='<%# Eval("ImagePath")%>' Height="35px" Width="30px" /></td>
                                                        <td>
                                                            <div class="plan-right-btn">
                                                                <asp:LinkButton ID="lbtnAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "A" ? true: false %>' OnClientClick="return confirm('Are you sure want to Deactive this Record?')" CommandName="Inactive" CssClass="courcebtn courcebtn-btn" CommandArgument='<%#Eval("M_Product_Id") %>'>Inactive</asp:LinkButton>
                                                                <asp:LinkButton ID="lbtniAccept" runat="server" Visible='<%# Eval("Flag").ToString() == "D" ? true: false %>' CommandName="Active" CssClass="coursebtn2 course-btn3" CommandArgument='<%#Eval("M_Product_Id") %>'>Active</asp:LinkButton>
                                                            </div>
                                                            <div class="plan-left-btn">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Modify" CssClass="coursebtn course-btn2" CommandArgument='<%#Eval("M_Product_Id") %>'>Edit</asp:LinkButton>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>

            </div>

        </div>

    </div>
</asp:Content>
