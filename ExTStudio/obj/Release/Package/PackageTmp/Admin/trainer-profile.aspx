﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="trainer-profile.aspx.cs" Inherits="ExTStudio.Admin.trainer_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgLogoImage.ClientID%>').prop('src', e.target.result)
                    .css('visibility', 'visible')
                };
                reader.readAsDataURL(input.files[0]);
                }
            }
    </script>

    <script>
        function validatenumerics(key) {
            var keycode = (key.which) ? key.which : key.keyCode;
            if (keycode > 31 && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else return true;
        }

        function validateMobileNo(form) {
            var tin = form.value;
            if (!isNumeric(form)) {
                return false;
            }
            else if (!(tin.length == 10)) {

                //  $.gritter.add({ title: 'Error Message', text: 'Please enter 10 digit Mobile No.' });
                //alert("Please enter 10 digit Mobile No.");

                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter 10 digit Mobile No.';
                form.value = "";
                form.focus();
                HideLabel();
                return false;
            }
        return true;
    }

    function checkEmail(sender) {
        var email = sender.value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            sender.value = "";
            sender.focus();
            //alert('Please Provide Proper Email Address');
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
            HideLabel();

            //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol required");
            return false;
        }
        else {
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
            return true();
        }
    }
    </script>

    <script>
        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var CompanyName = document.getElementById('<%=txtName.ClientID%>').value;
            var EmailId = document.getElementById('<%=txtEmailId.ClientID%>').value;
            var Mobile = document.getElementById('<%=txtMobile.ClientID%>').value;
            var About = document.getElementById('<%=txtAbout.ClientID%>').value;

            if (CompanyName == "" || CompanyName == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter name.';
                document.getElementById('<%=txtName.ClientID%>').focus();
                HideLabel();
                return false;
            }

            else if (EmailId == "" || EmailId == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter emailid.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Mobile == "" || Mobile == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter mobile no.';
                document.getElementById('<%=txtMobile.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (About == "" || About == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter about.';
                document.getElementById('<%=txtAbout.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">My Profile</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">

                            <div class="form-body">



                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <%-- <input type="text" class="form-control">--%>
                                            <asp:textbox id="txtName" class="form-control" runat="server" maxlength="100" placeholder="Enter Name"></asp:textbox>

                                        </div>
                                    </div>
                                    <!--/span-->


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>EmailId</label>
                                            <asp:textbox id="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)" maxlength="50" placeholder="Enter Email Address"></asp:textbox>

                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <asp:textbox id="txtMobile" class="form-control" runat="server" onchange="validateMobileNo(this)" onkeypress="return validatenumerics(event)" maxlength="10" placeholder="Enter Mobile No."></asp:textbox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>About</label>
                                            <asp:textbox id="txtAbout"  class="form-control" runat="server" placeholder="Enter About"></asp:textbox>

                                        </div>
                                    </div>

                                </div>
                                <!--/row-->


                                <div class="row">
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Image</label>
                                                    <asp:fileupload id="fuLogo" class="form-control ImageFile" onchange="ShowImagePreview(this)" runat="server"></asp:fileupload>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <img src="../images/no-image.png" id="imgLogoImage" class="ImagePrivew form-control" alt="" style="width: 150px; height: 150px" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div>

                                <div class="row no-margin">
                                    <div class="col-md-12">
                                        <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-9">
                                                <asp:linkbutton runat="server" id="lbtnSave" class="next" text="Save" onclientclick="return validateForm()" onclick="lbtnSave_Click"></asp:linkbutton>
                                                <asp:hiddenfield id="hnImagePath" runat="server" />
                                                <asp:hiddenfield id="hnId" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
