﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="change-password.aspx.cs" Inherits="ExTStudio.Admin.change_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function CheckPasswordStrenthCount() {
            var pass = document.getElementById('<%= txtNewPassword.ClientID %>');
            var Newpass = document.getElementById('<%= txtConfirmPassword.ClientID %>');
            if (pass.value != Newpass.value) {
                Newpass.value = "";
                Newpass.focus();
                //alert("Password Is Not Match");
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Password Is Not Match.';
                HideLabel();

                return false;
            }
        }
    </script>

    <script>
        function validateForm() {
           <%-- document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';--%>
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';

            var OPass = document.getElementById('<%=txtOldPassword.ClientID%>').value;
            var NPass = document.getElementById('<%=txtNewPassword.ClientID%>').value;
            var CPass = document.getElementById('<%=txtConfirmPassword.ClientID%>').value;

            if (OPass == "" || OPass == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter old password.';
                document.getElementById('<%=txtOldPassword.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (NPass == "" || NPass == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter new password.';
                document.getElementById('<%=txtNewPassword.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (CPass == "" || CPass == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter confirm password.';
                document.getElementById('<%=txtConfirmPassword.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (NPass != CPass) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Password and confirm passwrod not match.';
                document.getElementById('<%=txtConfirmPassword.ClientID%>').focus();
                HideLabel();
                return false;
            }


    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Change Password</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">

                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Old Password</label>
                                            <%-- <input type="text" class="form-control">--%>
                                            <asp:TextBox runat="server" ID="txtOldPassword" class="form-control" placeholder="Enter Old Password" TextMode="Password" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox runat="server" ID="txtNewPassword" class="form-control" placeholder="Enter New Password" TextMode="Password" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <%--<input type="text" class="form-control">--%>
                                            <asp:TextBox runat="server" ID="txtConfirmPassword" class="form-control" placeholder="Enter Confirm Password" TextMode="Password" MaxLength="50" onchange="CheckPasswordStrenthCount()"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                            <asp:LinkButton runat="server" ID="lbtnSave" class="next" Text="Change Password" OnClientClick="return validateForm()" OnClick="lbtnSave_Click"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
