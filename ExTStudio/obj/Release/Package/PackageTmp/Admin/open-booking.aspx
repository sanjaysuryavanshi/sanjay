﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="open-booking.aspx.cs" Inherits="ExTStudio.Admin.open_booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Open Bookings</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NAME</th>
                                            <th>Joining Fee</th>
                                            <th>Monthly Bookings</th>
                                            <th>IBAN</th>
                                            <th>TOTAL</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:ListView ID="repBooking" runat="server" OnItemCommand="repBooking_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><a href="reminder-message.aspx?MBookingId=<%# Eval("M_Booking_Id") %>"><%# Eval("Full_Name")%></a></td>
                                                    <td><%# Eval("Joining_Fee")%></td>
                                                    <td><%# Eval("Monthly_Booking")%></td>
                                                    <td><%# Eval("IBAN")%></td>
                                                    <td><%# Eval("Total")%></td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" Enabled='<%# Eval("IsPaid").ToString() == "A" ? true : false %>' runat="server" CommandName="Inactive" CssClass="monthly1 monthly2" CommandArgument='<%#Eval("M_Booking_Id") %>'>X</asp:LinkButton>
                                                        <%--<button class="monthly1 monthly2">Green</button></td>--%>
                                                </tr>

                                            </ItemTemplate>
                                        </asp:ListView>
                                        <%-- <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>

                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>
                                    <tr>
                                        <td>Lunar probe project</td>
                                        <td>500</td>
                                        <td>69</td>
                                        <td>DE 123 456</td>
                                        <td>900</td>
                                        <td>
                                            <button class="monthly1 monthly2">Green</button></td>
                                    </tr>--%>
                                    </tbody>

                                </table>
                            </div>
                        </div>


                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-md-3 joining-totl">
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <%-- <input type="text" class="form-control" placeholder="Total">--%>
                                            <asp:TextBox ID="txtFinalTotal" class="form-control" runat="server" MaxLength="10"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!--/row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


</asp:Content>
