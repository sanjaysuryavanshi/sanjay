﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="check-in.aspx.cs" Inherits="ExTStudio.Admin.check_in" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-wrapper" style="min-height: 232px;">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Check in</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">

                <asp:ListView ID="rptProbetraining" runat="server">
                    <ItemTemplate>
                        <a href="checkin-2.aspx?ProbId=<%# Eval("Member_Id") %>">
                            <div class="col-lg-3">
                                <div class="kart-img">
                                    <%--<div class="smily">
                                        <img src="assets/images/smil.png" />
                                    </div>--%>

                                    <asp:Image runat="server" src='<%# Eval("ImagePath") %>' Height="185px" Width="185px" />
                                </div>
                            </div>
                        </a>
                    </ItemTemplate>
                </asp:ListView>

                <%-- <div class="col-lg-3">
                    <div class="kart-img">
                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="kart-img">
                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="kart-img">
                        <div class="smily">
                            <img src="assets/images/smil.png" />
                        </div>

                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="kart-img">
                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="kart-img">
                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="kart-img">
                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="kart-img">
                        <img src="assets/images/kart.jpg" />
                    </div>
                </div>--%>
            </div>


        </div>

    </div>
</asp:Content>
