﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ExTStudio.Admin.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title>Studio-Admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="css/style.css" rel="stylesheet">

    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">

        function checkEmail(sender) {
            var email = sender.value;
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email)) {
                sender.value = "";
                sender.focus();
                //alert('Please Provide Proper Email Address');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
                HideLabel();

                //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
                //document.getElementById('emailSpan').removeAttribute("class");
                //document.getElementById('emailSpan').setAttribute("class", "symbol required");
                return false;
            }
            else {
                //document.getElementById('emailSpan').removeAttribute("class");
                //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
                return true();
            }
        }


    </script>

    <script>
        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';


            var EmailId = document.getElementById('<%=txtEmailId.ClientID%>').value;
            var Password = document.getElementById('<%=txtPassword.ClientID%>').value;

            if (EmailId == "" || EmailId == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter email id.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Password == "" || Password == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter password.';
                document.getElementById('<%=txtPassword.ClientID%>').focus();
                HideLabel();
                return false;
            }

        return true;
    }

    function validateForm1() {
        document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
             document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';


             var EmailId = document.getElementById('<%=txtFEmailId.ClientID%>').value;

             if (EmailId == "" || EmailId == undefined) {
                 document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter email id.';
                document.getElementById('<%=txtFEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            return true;
        }
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="preloader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                </svg>
            </div>

            <section id="wrapper">
                <div class="login-register" style="background-image: url(assets/images/background/login-register.jpg);">
                    <div class="login-box card card-2">
                        <div class="card-body">
                            <div role="form" class="form-horizontal form-material" id="loginform">
                                <h3 class="box-title m-b-20" style="text-align: center;">LOGIN</h3>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%-- <input class="form-control" type="text" required="" placeholder="Username">--%>
                                        <asp:TextBox ID="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)" MaxLength="150" placeholder="Email Address"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <%--<input class="form-control" type="password" required="" placeholder="Password">--%>
                                        <asp:TextBox ID="txtPassword" TextMode="Password" class="form-control" runat="server" MaxLength="50" placeholder="Password"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="checkbox checkbox-primary pull-left p-t-0" style="margin-left: -12px;">
                                            <input id="checkbox-signup" type="checkbox">
                                            <%--<label for="checkbox-signup"> Remember me </label>--%>
                                            <asp:CheckBox runat="server" ID="chkTrainer" Text="Are you Trainer ?" />
                                        </div>
                                        <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i>Forgot Password?</a>
                                    </div>
                                </div>
                                <div class="form-group text-center m-t-20">
                                    <div class="col-xs-12">
                                        <%--<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>--%>
                                        <asp:LinkButton runat="server" ID="lbtnLogIn" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" Text="Log In" OnClientClick="return validateForm()" OnClick="lbtnLogIn_Click"></asp:LinkButton>

                                    </div>
                                </div>
                                <!--<div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                <div class="social">
                                    <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                    <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                                </div>
                            </div>
                        </div>-->
                                <div class="form-group m-b-0">
                                    <div class="col-sm-12 text-center">
                                        <p><a href="create-new-account.aspx" class="text-info m-l-5"><b>Create a New Account</b></a></p>
                                    </div>
                                </div>
                            </div>
                            <div role="form" class="form-horizontal" id="recoverform">
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <h3>Recover Password</h3>
                                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-12">
                                            <asp:CheckBox runat="server" ID="chkFTrainer" Text="Are you Trainer ?" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <%-- <input class="form-control" type="text" required="" placeholder="Email">--%>
                                        <asp:TextBox ID="txtFEmailId" class="form-control" runat="server" onchange="checkEmail(this)" MaxLength="150" placeholder="Email Address"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group text-center m-t-20">
                                    <div class="col-xs-12">
                                        <%--<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>--%>
                                        <asp:LinkButton runat="server" ID="lbtnReset" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" Text="Reset" OnClientClick="return validateForm1()" OnClick="lbtnReset_Click"></asp:LinkButton>

                                    </div>
                                </div>
                            </div>

                            <div class="row no-margin">
                                <div class="col-md-12">
                                    <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>

    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
