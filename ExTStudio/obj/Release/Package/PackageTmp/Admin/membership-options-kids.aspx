﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="membership-options-kids.aspx.cs" Inherits="ExTStudio.Admin.membership_options_kids" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function AllowFloat(sender) {
            if (!sender.value.match(/^[0-9.]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warning('Please Enter only Decimal in text', 'Invalid Text');
                //alert('Please Enter only Decimal in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only Decimal in text.';
                HideLabel();
            }
        }

        function validatenumerics(key) {
            var keycode = (key.which) ? key.which : key.keyCode;
            if (keycode > 31 && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else return true;
        }

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var Pname = document.getElementById('<%=txtPackageName.ClientID%>').value;
            var PTime = document.getElementById('<%=txtPackageTime.ClientID%>').value;
            var TW = document.getElementById('<%=txtTimesPerWeek.ClientID%>').value;
            var MI = document.getElementById('<%=txtMonthlyInvestment.ClientID%>').value;

            if (Pname == "" || Pname == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter package name.';
                document.getElementById('<%=txtPackageName.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (PTime == "" || PTime == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter package time.';
                document.getElementById('<%=txtPackageTime.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (TW == "" || TW == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter times per week.';
                document.getElementById('<%=txtTimesPerWeek.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (MI == "" || MI == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter monthly investment.';
                document.getElementById('<%=txtMonthlyInvestment.ClientID%>').focus();
                HideLabel();
                return false;
            }

    return true;
}
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Membership options/kids</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>


        <div class="col-md-8 ">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-outline-info">

                        <div class="card-body">
                            <div role="form">
                                <div class="form-body">
                                    <%-- <h3>Package 1
                                        <br />
                                        12 Month</h3>--%>

                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Package Name</label>
                                                <%--  <input type="text" class="form-control">--%>
                                                <asp:TextBox runat="server" ID="txtPackageName" MaxLength="50" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Package Time</label>
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:TextBox runat="server" ID="txtPackageTime" MaxLength="50" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Starter Price</label>
                                                <%-- <input type="text" class="form-control">--%>
                                                <asp:TextBox runat="server" ID="txtStarter" onkeypress="return AllowFloat(this)" MaxLength="10" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Times Per Week</label>
                                                <asp:TextBox runat="server" ID="txtTimesPerWeek" MaxLength="50" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label>Monthly Investment</label>
                                                <%-- <input type="text" class="form-control">--%>
                                                <asp:TextBox runat="server" ID="txtMonthlyInvestment" onkeypress="return AllowFloat(this)" MaxLength="10" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row no-margin">
                                        <div class="col-md-12">
                                            <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-md-12 trainig-next trainig-next2">
                                    <%-- <a href="#" class="next">Next</a>--%>
                                    <asp:LinkButton runat="server" ID="lbtnNext" class="next" Text="Next" OnClientClick="return validateForm()" OnClick="lbtnNext_Click"></asp:LinkButton>
                                    <asp:HiddenField runat="server" ID="hdnId"/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>

</asp:Content>
