﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="membership-option-select.aspx.cs" Inherits="ExTStudio.Admin.membership_option_select" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function validatenumerics(key) {
            var keycode = (key.which) ? key.which : key.keyCode;
            if (keycode > 31 && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else return true;
        }

        function AllowAlphabetSpace(sender) {
            if (!sender.value.match(/^[a-zA-Z ._-]+$/) && sender.value != "") {
                sender.value = "";
                sender.focus();
                //toastr.warning('Please Enter only alphabets in text', 'Invalid Text');
                //alert('Please Enter only alphabets in text');
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Enter only alphabets in text.';
                HideLabel();

            }
        }

        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            if (document.getElementById('<%=ddlMOption.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select membership option.';
                document.getElementById('<%=ddlMOption.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }

            return true;
        }
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Membership Option</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class=" card-outline-info">

                        <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                                    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                        <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="rblKA" />
                                <asp:AsyncPostBackTrigger ControlID="ddlMOption" />
                                <asp:PostBackTrigger ControlID="lbtnSave" />
                                <asp:PostBackTrigger ControlID="lbtnDownload" />
                                

                            </Triggers>
                            <ContentTemplate>

                                <div class="card-body">
                                    <div role="form" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <%--<input type="text" class="form-control" placeholder="Product Name">--%>
                                                            <asp:RadioButtonList ID="rblKA" runat="server" OnSelectedIndexChanged="rblKA_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Kide" Value="K"></asp:ListItem>
                                                                <asp:ListItem Text="Adult" Value="A"></asp:ListItem>
                                                            </asp:RadioButtonList>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="col-md-5">
                                                    <div class="form-group row">

                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddlMOption" class="form-control" runat="server" OnSelectedIndexChanged="ddlMOption_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>

                                        <div class="row no-margin">
                                            <div class="col-md-12">
                                                <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-12">

                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Starter Price</label><br />
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:Label ID="lblStarterPrice" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Package Time</label><br />
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:Label ID="lblPackageTime" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Times Per Week</label><br />
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:Label ID="lblTimesPerWeek" runat="server"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Monthly Investment</label><br />
                                                <%--<input type="text" class="form-control">--%>
                                                <asp:Label ID="lblMonthlyInvestment" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="container-fluid">
                                        <div class="row">
                                            <asp:ListView ID="repProduct" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-lg-3">
                                                        <div class="kart-img">

                                                            <asp:Image runat="server" src='<%# Eval("ImagePath") %>' Height="200px" Width="150px" />
                                                            <br />
                                                            <asp:Label runat="server" Text='<%# Eval("Product_Name")%>' />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                            <div class="col-md-9 save-btn" style="float: left;">
                                                <%-- <a href="#" class="btn btn-success">Orer Now</a>--%>
                                                <asp:LinkButton runat="server" ID="lbtnSave" class="btn btn-success" Text="Save" OnClick="lbtnSave_Click" OnClientClick="return validateForm()"></asp:LinkButton>

                                            </div>
                                            <div class="col-md-3" style="float: right;">
                                                <asp:LinkButton runat="server" ID="lbtnDownload" class="btn btn-success" Text="Download Agreement" OnClick="lbtnDownload_Click"></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                    </div>
                </div>
            </div>
        </div>

    </div>

    </div>
    <%--<CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />--%>
    <%--<CR:CrystalReportViewer runat="server" ID="cr1" />--%>
    </div>
</asp:Content>
