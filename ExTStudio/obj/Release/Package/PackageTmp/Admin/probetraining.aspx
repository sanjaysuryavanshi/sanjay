﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/master-page.Master" AutoEventWireup="true" CodeBehind="probetraining.aspx.cs" Inherits="ExTStudio.Admin.probetraining" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function validateMobileNo(form) {
            var tin = form.value;
            if (!isNumeric(form)) {
                return false;
            }
            else if (!(tin.length == 10)) {

                //  $.gritter.add({ title: 'Error Message', text: 'Please enter 10 digit Mobile No.' });
                //alert("Please enter 10 digit Mobile No.");

                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter 10 digit Mobile No.';
                form.value = "";
                form.focus();
                HideLabel();
                return false;
            }
        return true;
    }

    function validatenumerics(key) {
        var keycode = (key.which) ? key.which : key.keyCode;
        if (keycode > 31 && (keycode < 48 || keycode > 57)) {
            return false;
        }
        else return true;
    }

    function checkEmail(sender) {
        var email = sender.value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            sender.value = "";
            sender.focus();
            //alert('Please Provide Proper Email Address');
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please Provide Proper Email Address.';
            HideLabel();

            //toastr.warning('Please Provide Proper Email Address', 'Invalid Email Address');
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol required");
            return false;
        }
        else {
            //document.getElementById('emailSpan').removeAttribute("class");
            //document.getElementById('emailSpan').setAttribute("class", "symbol ok");
            return true();
        }
    }

    </script>

    <script>
        function validateForm() {
            document.getElementById('<%=msgerror.ClientID%>').style.display = 'none';
            document.getElementById('<%=msgsuccess.ClientID%>').style.display = 'none';

            var Fname = document.getElementById('<%=txtFName.ClientID%>').value;
            var Lname = document.getElementById('<%=txtLName.ClientID%>').value;
            var Mobile = document.getElementById('<%=txtMobileNo.ClientID%>').value;
            var Email = document.getElementById('<%=txtEmailId.ClientID%>').value;
            var Age = document.getElementById('<%=txtAge.ClientID%>').value;
            <%--var Heard = document.getElementById('<%=txtHeard.ClientID%>').value;--%>
           <%-- var GorFB = document.getElementById('<%=txtGorFB.ClientID%>').value;--%>

            if (document.getElementById('<%=ddlLocation.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please select location.';
                document.getElementById('<%=ddlLocation.ClientID%>').focus();
                HideLabel();
                // alert("Please Select Job Title.");
                return false;
            }

            else if (Fname == "" || Fname == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter student first name.';
                document.getElementById('<%=txtFName.ClientID%>').focus();
                HideLabel();
                return false;
            }

            else if (Fname == "" || Fname == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter student last name.';
                document.getElementById('<%=txtLName.ClientID%>').focus();
                HideLabel();
                return false;
            }

            else if (Mobile == "" || Mobile == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter mobile no.';
                document.getElementById('<%=txtMobileNo.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Email == "" || Email == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter email id.';
                document.getElementById('<%=txtEmailId.ClientID%>').focus();
                HideLabel();
                return false;
            }
            else if (Age == "" || Age == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter age.';
                document.getElementById('<%=txtAge.ClientID%>').focus();
                HideLabel();
                return false;
            }
            <%-- else if (Heard == "" || Heard == undefined) {
                document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter heard.';
                document.getElementById('<%=txtHeard.ClientID%>').focus();
                HideLabel();
                return false;
            }--%>
            <%-- else if (GorFB == "" || GorFB == undefined) {
                 document.getElementById('<%=msgerror.ClientID%>').style.display = 'block';
                 document.getElementById('<%=lblErrorMsg.ClientID%>').innerHTML = ' Please enter google/facebook.';
                 document.getElementById('<%=txtGorFB.ClientID%>').focus();
                 HideLabel();
                 return false;
             }--%>

            return true;
        }
    </script>

    <script>
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=msgsuccess.ClientID %>").style.display = "none";
                document.getElementById("<%=msgerror.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Probetraining</h3>
            </div>
            <!--<div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>-->
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>


        <div class="col-md-8 chart-prob">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-outline-info prob-table" id="myDIV" style="display: block;">

                        <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="up1">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFF; opacity: 0.7;">
                                    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                        <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlLocation" />
                            </Triggers>
                            <ContentTemplate>

                                <div class="card-body">
                                    <div role="form">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>Location Name</label>
                                                    <%-- <input type="text" class="form-control">--%>
                                                    <asp:DropDownList runat="server" ID="ddlLocation" class="form-control" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>Trainer Name</label>
                                                    <%-- <input type="text" class="form-control">--%>
                                                    <asp:DropDownList runat="server" ID="ddlTrainer" class="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Name Of Student</label>
                                                    <%-- <input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtFName" class="form-control" runat="server" placeholder="Enter First Name" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>

                                                    <%-- <input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtLName" class="form-control" runat="server" placeholder="Enter Last Name" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>Tel#</label>
                                                    <%--<input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtMobileNo" class="form-control" runat="server" onchange="validateMobileNo(this)" onkeypress="return validatenumerics(event)" MaxLength="10"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <%--<input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtEmailId" class="form-control" runat="server" onchange="checkEmail(this)" MaxLength="150"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Age</label>
                                                    <%--<input type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtAge" class="form-control" runat="server" onkeypress="return validatenumerics(event)" MaxLength="3"></asp:TextBox>

                                                </div>
                                            </div>
                                            <!--/span-->
                                            <%-- <div class="col-md-4">
                                        <div class="form-group">
                                            <label>How Heard ?</label>
                                            <%--  <input type="text" class="form-control">
                                            <%--<asp:TextBox ID="txtHeard" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>--%>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>How Heard ?</label>
                                                    <asp:RadioButtonList ID="rblGorFb" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="Google" Value="Google" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Facebook" Value="Facebook"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <%-- <input type="text" class="form-control">--%>
                                                    <%--<asp:TextBox ID="txtGorFB" class="form-control" runat="server"></asp:TextBox>--%>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-12">
                                    <%--<div class="chart">

                                <svg height="355" version="1.1" width="649.328" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;">
                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.84375" y="316" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
                                    </text><path fill="none" stroke="#e0e0e0" d="M45.34375,316H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.84375" y="243.25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">75</tspan>
                                    </text><path fill="none" stroke="#e0e0e0" d="M45.34375,243.25H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.84375" y="170.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">150</tspan>
                                    </text><path fill="none" stroke="#e0e0e0" d="M45.34375,170.5H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.84375" y="97.75" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">225</tspan>
                                    </text><path fill="none" stroke="#e0e0e0" d="M45.34375,97.75H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.84375" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">300</tspan>
                                    </text><path fill="none" stroke="#e0e0e0" d="M45.34375,25H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="624.328" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Sunday</tspan>
                                    </text><text x="527.654534899635" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Saturday</tspan>
                                    </text><text x="431.2452049498175" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Friday</tspan>
                                    </text><text x="334.835875" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Thursday</tspan>
                                    </text><text x="238.42654505018248" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Wednesday</tspan>
                                    </text><text x="141.7530799498175" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Tuesday</tspan>
                                    </text><text x="45.34375" y="328.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Monday</tspan>
                                    </text><path fill="#3c8dde" stroke="none" d="M45.34375,267.5C69.44608248745438,248.1,117.65074746236313,193.5325239398085,141.7530799498175,189.9C165.92144622490875,186.2575239398085,214.25817877509124,231.11504787961695,238.42654505018248,238.4C262.52887753763684,245.66504787961696,310.7335425125456,260.225,334.835875,248.10000000000002C358.93820748745435,235.97500000000002,407.14287246236313,145.64375,431.2452049498175,141.4C455.34753743727185,137.15625,503.55220241218063,222.62588919288646,527.654534899635,214.15C551.8229011747262,205.65088919288647,600.1596337249088,108.6625,624.328,73.5L624.328,316L45.34375,316Z" fill-opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0;"></path><path fill="none" stroke="#1976d2" d="M45.34375,267.5C69.44608248745438,248.1,117.65074746236313,193.5325239398085,141.7530799498175,189.9C165.92144622490875,186.2575239398085,214.25817877509124,231.11504787961695,238.42654505018248,238.4C262.52887753763684,245.66504787961696,310.7335425125456,260.225,334.835875,248.10000000000002C358.93820748745435,235.97500000000002,407.14287246236313,145.64375,431.2452049498175,141.4C455.34753743727185,137.15625,503.55220241218063,222.62588919288646,527.654534899635,214.15C551.8229011747262,205.65088919288647,600.1596337249088,108.6625,624.328,73.5" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.34375" cy="267.5" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="141.7530799498175" cy="189.9" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="238.42654505018248" cy="238.4" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="334.835875" cy="248.10000000000002" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="431.2452049498175" cy="141.4" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="527.654534899635" cy="214.15" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="624.328" cy="73.5" r="3" fill="#1976d2" stroke="#1976d2" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#59ccda" stroke="none" d="M45.34375,238.4C69.44608248745438,233.55,117.65074746236313,216.578317373461,141.7530799498175,219C165.92144622490875,221.42831737346103,214.25817877509124,269.9415868673051,238.42654505018248,257.8C262.52887753763684,245.69158686730506,310.7335425125456,132.9125,334.835875,122C358.93820748745435,111.0875,407.14287246236313,158.375,431.2452049498175,170.5C455.34753743727185,182.625,503.55220241218063,219,527.654534899635,219C551.8229011747262,219,600.1596337249088,182.625,624.328,170.5L624.328,316L45.34375,316Z" fill-opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0;"></path><path fill="none" stroke="#26c6da" d="M45.34375,238.4C69.44608248745438,233.55,117.65074746236313,216.578317373461,141.7530799498175,219C165.92144622490875,221.42831737346103,214.25817877509124,269.9415868673051,238.42654505018248,257.8C262.52887753763684,245.69158686730506,310.7335425125456,132.9125,334.835875,122C358.93820748745435,111.0875,407.14287246236313,158.375,431.2452049498175,170.5C455.34753743727185,182.625,503.55220241218063,219,527.654534899635,219C551.8229011747262,219,600.1596337249088,182.625,624.328,170.5" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.34375" cy="238.4" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="141.7530799498175" cy="219" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="238.42654505018248" cy="257.8" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="334.835875" cy="122" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="431.2452049498175" cy="170.5" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="527.654534899635" cy="219" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="624.328" cy="170.5" r="3" fill="#26c6da" stroke="#26c6da" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg>
                            </div>--%>
                                    <asp:Label ID="lblDays" runat="server"></asp:Label>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="col-lg-12">
                            <%-- <a href="#" class="next">Next</a>--%>
                            <asp:LinkButton runat="server" ID="lbtnNext" class="next" Text="Save" OnClientClick="return validateForm()" OnClick="lbtnNext_Click"></asp:LinkButton>
                        </div>

                    </div>

                    <div class="row no-margin">
                        <div class="col-md-12">
                            <div class="msgsuccess" style="display: none" id="msgsuccess" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><i class="fa fa-check"></i></span><span id="lblSuccessMsg" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="msgerror" style="display: none" id="msgerror" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><i class="fa fa-exclamation-triangle"></i></span><span id="lblErrorMsg" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>




    </div>
</asp:Content>
