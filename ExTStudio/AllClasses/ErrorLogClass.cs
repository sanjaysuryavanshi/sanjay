﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ExTStudio.AllClasses
{
    public class ErrorLogClass
    {
       

        InteractionMethod objInteraction = new InteractionMethod();
        Prop objValue = new Prop();

        public void ErrorLogInsert(String ErrorStatusCode, String ExcType, String ExceptionMsg, String ExTrace, String ExSource, String ApiUrl)
        {
            objValue.Flag = "A";
            objValue.ErrorStatusCode = ErrorStatusCode;
            objValue.ExceptionMsg = ExceptionMsg;
            objValue.ExTrace = ExTrace;
            objValue.ExSource = ExSource;
            objValue.ApiUrl = ApiUrl;
            objValue.ExcType = ExcType;
            objInteraction.InsertError(objValue);
        }

    }
}