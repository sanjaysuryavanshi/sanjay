﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Hosting;

namespace ExTStudio.AllClasses
{
    public class ComanClass
    {
        #region Variable
        InteractionMethod objInteraction = new InteractionMethod();
        Prop objvalue = new Prop();
        DataSet dsEmail, ds = new DataSet();
        int rowEfected = new int();
        String[] rowEffectedarr;
        ErrorLogClass E = new ErrorLogClass();
        int PortNo = 25;
        String SMSOnOff = System.Configuration.ConfigurationSettings.AppSettings["SMSOnOff"];
        String EmailOnOff = System.Configuration.ConfigurationSettings.AppSettings["EmailOnOff"];
        #endregion

        #region Send Email No Replay Customer
        public void SendMailNoReplayCustomer(String ToCustomerEmail, String Title, String subject, String Body, String Type)
        {
            if (EmailOnOff.ToUpper().ToString() == "ON")
            {
                objvalue.EmailType = Type;
                dsEmail = objInteraction.EmailConfigrationSelectByEmailType(objvalue);
                string Host = dsEmail.Tables[0].Rows[0]["Host"].ToString();
                decimal Port = Convert.ToDecimal(dsEmail.Tables[0].Rows[0]["Port"].ToString());
                string CompanyEmail = dsEmail.Tables[0].Rows[0]["UserName"].ToString();
                string Password = dsEmail.Tables[0].Rows[0]["Password"].ToString();
                string SSL = dsEmail.Tables[0].Rows[0]["SSL"].ToString();
                string DispayName = "Methnic";  //company name
                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(CompanyEmail, DispayName);
                // Recipient e-mail address.
                Msg.To.Add(ToCustomerEmail);
                objvalue.Flag = "M";
                StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Email.html"));
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                StrContent = StrContent.Replace("{Title}", Title);
                StrContent = StrContent.Replace("{Message}", Body);
                Msg.Subject = subject;
                Msg.Body = StrContent;
                Msg.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                //smtp.Port = Convert.ToInt32(Port);
                smtp.Port = Convert.ToInt32(PortNo);
                smtp.Credentials = new System.Net.NetworkCredential(CompanyEmail, Password);
                smtp.EnableSsl = Convert.ToBoolean(SSL);
                smtp.Send(Msg);
            }
        }
        #endregion

        #region Send Email with Attachments
        public void SendMailWithAttachments(String ToCustomerEmail, String Title, String subject, String Body, String Type, String FileName)
        {
            if (EmailOnOff.ToUpper().ToString() == "ON")
            {
                objvalue.EmailType = Type;
                dsEmail = objInteraction.EmailConfigrationSelectByEmailType(objvalue);
                string Host = dsEmail.Tables[0].Rows[0]["Host"].ToString();
                decimal Port = Convert.ToDecimal(dsEmail.Tables[0].Rows[0]["Port"].ToString());
                string CompanyEmail = dsEmail.Tables[0].Rows[0]["UserName"].ToString();
                string Password = dsEmail.Tables[0].Rows[0]["Password"].ToString();
                string SSL = dsEmail.Tables[0].Rows[0]["SSL"].ToString();
                string DispayName = "Studio";  //company name

                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(CompanyEmail, DispayName);
                // Recipient e-mail address.
                Msg.To.Add(ToCustomerEmail);

                objvalue.Flag = "M";

                StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Email.html"));
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                StrContent = StrContent.Replace("{Title}", Title);
                StrContent = StrContent.Replace("{Message}", Body);
                Msg.Subject = subject;
                Msg.Body = StrContent;
                Msg.IsBodyHtml = true;

                if (!String.IsNullOrEmpty(Convert.ToString(FileName).Trim()))
                {
                    String f = Path.GetFileName(FileName);
                    String pdfLocalFileName = System.Web.HttpContext.Current.Server.MapPath("~/PDFs/ReminderMsgPDFs/" + f);
                    //var stream = new WebClient().OpenRead(FileName);
                    //Attachment attachement = new Attachment(stream, "");
                    // var client = new WebClient();
                    // Download the PDF file from external site (pdfUrl) 
                    // to your local file system (pdfLocalFileName)
                    // client.DownloadFile(FileName, pdfLocalFileName);

                    Attachment attachment = new Attachment(pdfLocalFileName, MediaTypeNames.Application.Pdf);
                    Msg.Attachments.Add(attachment);
                }

                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                //smtp.Port = Convert.ToInt32(Port);
                smtp.Port = Convert.ToInt32(PortNo);
                smtp.Credentials = new System.Net.NetworkCredential(CompanyEmail, Password);
                smtp.EnableSsl = Convert.ToBoolean(SSL);
                smtp.Send(Msg);
            }
        }
        #endregion

        #region Send SMS
        public void SendSmsTransaction(String ToNumber, String Body)
        {
            if (SMSOnOff.ToUpper().ToString() == "ON")
            {
                objvalue.ApiFlag = "T";
                ds = objInteraction.SelectByApiFlagSmsApi(objvalue);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        String url = "";
                        //int smscount = Body.Length;

                        url = ds.Tables[0].Rows[0]["ApiURL"].ToString().Trim() + "user=" + ds.Tables[0].Rows[0]["UserName"].ToString().Trim() + "&pass=" + ds.Tables[0].Rows[0]["Password"].ToString().Trim() + "&sender=" + ds.Tables[0].Rows[0]["SenderID"].ToString().Trim() + "&phone=" + ToNumber + "&text=" + Body + "&priority=" + ds.Tables[0].Rows[0]["Priorty"].ToString().Trim() + "&stype=" + ds.Tables[0].Rows[0]["Type"].ToString().Trim();
                        //"http://bhashsms.com/api/sendmsg.php?user=Success&pass=654321&sender=BSHSMS&phone=" + context.Request.Cookies["LoginEventRushUserCookies"]["MobileNo"].ToString() + "&text=" + msg + "&priority=ndnd&stype=normal"
                        string wholePage = null;
                        using (WebClient webClient = new WebClient())
                        {
                            Stream stream = webClient.OpenRead(url);
                            StreamReader streamReader = new StreamReader(stream);
                            wholePage = streamReader.ReadToEnd();
                            streamReader.Close();
                            stream.Close();
                        }
                    }
                }

                //Operations.SMSLogInsert(Convert.ToDecimal(ToNumber), Body, Convert.ToInt64(0), "P");
            }
        }
        #endregion
    }
}